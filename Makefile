types:
	bash scripts/types.sh

doc:
	markdown README.md | sed 's|public/||g' | tee docs/README.html
	vhdocl -o public -l daq_core --nosourcelinks --homehtml=docs/README.html autogen pkg src

