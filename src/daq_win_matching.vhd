library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library daq_core;
use daq_core.yml2hdl.all;
use daq_core.daq_v3.all;
use daq_core.daq_defs.all;

--* @author Thiago Costa de Paiva
--* @id $git$
--* @version $git$
--*
--* @brief Create the structure needed for the header and streams to assemble
--* the final packet.
--*
--* The window matching engine block instantiate nodes for the header, where
--* the common information is kept, and all the streams, where the filtered
--* data is stored. It also coordinates the activity happening to inform the
--* packet builder when the selection process is done and also to release
--* itself when there is no data anymore to be retrieved.

entity daq_win_matching is
  generic (
    --* Array of stream widths to be used in header
    g_STREAM_WIDTH_ARRAY                : integer_vector;
    --* Array of depth values to configure data FIFOs
    g_FIFO_DEPTH_ARRAY                  : integer_vector;
    --* Array of memory types to configure data FIFOs
    g_FIFO_MEMORY_TYPE_ARRAY            :  string_vector;
    --* Number of bits used in the header to describe the data FIFO depth
    g_NBITS_STREAM_HDR_FIFO_DEPTH_ARRAY : integer_vector;
    --* Number of bits used in header for each stream header
    g_NBITS_STREAM_HDR_WIDTH_ARRAY      : integer_vector;
    --* Number of bits used in header for each data FIFO counter
    g_NBITS_STREAM_HDR_COUNTER_ARRAY    : integer_vector;
    --* Number of bits used in header for each user stream header
    g_NBITS_STREAM_HDR_USER_ARRAY       : integer_vector;
    --* Number of bits used in header to represent the status of window
    --* matching engines
    g_NBITS_SNAPSHOT                    : integer;
    --* Format version number
    g_FORMAT_VERSION                    : natural;
    g_GHDL                              : boolean;
    g_ID                                : integer
  );
  -----------------------------------------------------------------------------
  port (
    --* fast clock
    i_sys_clk_fast : in std_logic;
    --*  system reset
    i_sys_rst      : in std_logic;
    --* id counter strobe
    i_sys_bx       : in std_logic;
    -----------------------------------------------------------------------
    --* ID counter
    i_bcid_cnt : in unsigned(11 downto 0);
    -----------------------------------------------------------------------
    --* status of window matching engines
    i_use_snapshot   : in std_logic_vector;
    ------------------------------------------------------------------------
    --* request strobe
    i_req_strb        : in std_logic;
    --* value of the calculated opening window edge to be used
    i_win_opening_cnt : in unsigned(11 downto 0);
    --* value of the calculated request counter to be used
    i_win_request_cnt : in unsigned(11 downto 0);
    --* value of the calculated closing window edge to be used
    i_win_closing_cnt : in unsigned(11 downto 0);
    ----------------------------------------------------------------------------
    --* payload for the first bits of the packet
    i_sys_hdr            : in std_logic_vector;
    --* current event id to be used
    i_event_id           : in unsigned(31 downto 0);
    --* id counter value when the request strobe is delivered to the window
    --* matching window
    i_curr_bcid          : in unsigned(11 downto 0);
    --* value of the window opening offset when the request strobe was received
    i_win_opening_offset : in unsigned(11 downto 0);
    --* value of the request offset when the request strobe was received
    i_win_request_offset : in unsigned(11 downto 0);
    --* value of the window closing offset when the request strobe was received
    i_win_closing_offset : in unsigned(11 downto 0);
    --* value of the window timeout when the request strobe was received
    i_win_window_timeout : in unsigned(11 downto 0);
    --* value of the busy threshold when the request strobe was received
    i_win_busy_threshold : in unsigned( 7 downto 0);
    ------------------------------------------------------------------------
    --* payload of the user header when the request strobe was received
    i_usr_hdr_payload : in std_logic_vector;
    ------------------------------------------------------------------------
    --* concatenated bitvector of all stream user headers
    i_stream_user_header_emurray : in std_logic_vector;
    --* array with control id counter values for each data stream
    i_stream_ctrl_bcid_array     : in bcid_array_t;
    --* array with data id counter values for each data stream
    i_stream_data_bcid_array     : in bcid_array_t;
    --* array with valid strobes for each data stream
    i_stream_valid_array         : in std_logic_vector;
    --* array with enables for each data stream
    i_stream_enable_array        : in std_logic_vector;
    --* concatenated bitvector of all stream data payloads
    i_stream_payload_emurray     : in std_logic_vector;
    ------------------------------------------------------------------------
    --* full flag from header bus converter
    i_hdr_full       :  in std_logic;
    --* not empty flag to the header bus converter
    o_hdr_nempty     : out std_logic;
    --* write strobe to the header bus converter FIFO
    o_hdr_wr_strb    : out std_logic;
    --* data payload to the header bus converter FIFO
    o_hdr_payload    : out std_logic_vector;
    --* data ready flag with respect to the header acquisition
    o_hdr_data_ready : out std_logic;
    ------------------------------------------------------------------------
    --* full flag from stream header bus converter
    i_stream_hdr_full_array       :  in std_logic_vector;
    --* not empty flag to the stream header bus converter
    o_stream_hdr_nempty_array     : out std_logic_vector;
    --* write strobe to the stream header bus converter FIFO
    o_stream_hdr_wr_strb_array    : out std_logic_vector;
    --* data payload to the stream header bus converter FIFO
    o_stream_hdr_payload_emurray  : out std_logic_vector;
    --* data ready flag with respect to the stream header acquisition
    o_stream_hdr_data_ready_array : out std_logic_vector;
    ------------------------------------------------------------------------
    --* full flag from stream body bus converter
    i_stream_body_full_array       :  in  std_logic_vector;
    --* not empty flag to the stream body bus converter
    o_stream_body_nempty_array     : out  std_logic_vector;
    --* write strobe to the stream body bus converter FIFO
    o_stream_body_wr_strb_array    : out  std_logic_vector;
    --* data payload to the stream body bus converter FIFO
    o_stream_body_data_ready_array : out  std_logic_vector;
    --* data ready flag with respect to the stream body acquisition
    o_stream_body_payload_emurray  : out std_logic_vector;
    ------------------------------------------------------------------------
    --* flag indicating that data from all nodes are ready
    o_data_ready    : out std_logic;
    o_nodes_empty   : out std_logic;
    --* flag indicating that there is still data to be retrieved from nodes
    o_nempty_array  : out std_logic_vector;
    ------------------------------------------------------------------------
    --* flag indicating that this window matching engine is processing
    --* information
    o_wm_active : out std_logic
  );
end entity daq_win_matching;

architecture V2 of daq_win_matching is

  signal i_stream_user_header_emurray_alias  : std_logic_vector(i_stream_user_header_emurray'high  downto i_stream_user_header_emurray'low );
  signal i_stream_payload_emurray_alias      : std_logic_vector(i_stream_payload_emurray'high      downto i_stream_payload_emurray'low     );
  signal o_stream_hdr_payload_emurray_alias  : std_logic_vector(o_stream_hdr_payload_emurray'high  downto o_stream_hdr_payload_emurray'low );
  signal o_stream_body_payload_emurray_alias : std_logic_vector(o_stream_body_payload_emurray'high downto o_stream_body_payload_emurray'low);

  
  --* Function to calculate the width of the stream header in the final packet
  function STREAM_HDR_WIDTH_ARRAY return integer_vector is
    variable y : integer_vector(g_STREAM_WIDTH_ARRAY'range);
  begin

    for k in g_STREAM_WIDTH_ARRAY'range loop
      y(k) := g_NBITS_STREAM_HDR_FIFO_DEPTH_ARRAY(k);
      y(k) := y(k) + g_NBITS_STREAM_HDR_WIDTH_ARRAY(k);
      y(k) := y(k) + g_NBITS_STREAM_HDR_COUNTER_ARRAY(k);
      y(k) := y(k) + g_NBITS_STREAM_HDR_USER_ARRAY(k);
    end loop;

    return y;
  end function STREAM_HDR_WIDTH_ARRAY;

  --* shortcut to get the number of streams
  constant STREAMS : integer := g_STREAM_WIDTH_ARRAY'length;

  --* IDLE: waiting activation
  --*
  --* COLLECT: nodes processing data
  --*
  --* SEND: waiting for pkt bldr to retrieve data
  type wm_state_t is (
    IDLE,
    COLLECT,
    SEND
  );
  signal state : wm_state_t := IDLE;

  signal use_snapshot : std_logic_vector(i_use_snapshot'range);


  signal wm_active     : std_logic := '0';
  signal wm_data_ready : std_logic := '0';
  signal wm_timed_out  : std_logic := '0';

  signal wm_opening : unsigned(11 downto 0) := (others => '0');
  signal wm_request : unsigned(11 downto 0) := (others => '0'); -- daq bcid
  signal wm_closing : unsigned(11 downto 0) := (others => '0');
  signal wm_timeout : unsigned(11 downto 0) := (others => '0');

  signal curr_bcid : unsigned(11 downto 0) := (others => '0');

  signal win_opening_offset : unsigned(11 downto 0);
  signal win_request_offset : unsigned(11 downto 0);
  signal win_closing_offset : unsigned(11 downto 0);
  signal win_window_timeout : unsigned(11 downto 0);
  signal win_busy_threshold : unsigned( 7 downto 0);

  signal ttc_bcid    : unsigned(11 downto 0);
  signal ttc_evid    : unsigned(11 downto 0);
  signal ttc_orid    : unsigned(11 downto 0);

  signal all_data_ready : std_logic;
  signal all_nodes_empty : std_logic;

  -- header node signals -------------------------------------------------------
  signal hnode_error      : std_logic;
  signal hnode_data_ready : std_logic;
  -- signal hdr_busy       : std_logic;
  signal hdr_ce       : std_logic;

  -- data node signals ---------------------------------------------------------
  signal dnodes_error             : std_logic_vector(g_STREAM_WIDTH_ARRAY'range);
  signal dnodes_data_ready        : std_logic_vector(g_STREAM_WIDTH_ARRAY'range);
  -- signal stream_hdr_busy_array  : std_logic_vector(g_STREAM_WIDTH_ARRAY'range);
  signal stream_hdr_ce_array  : std_logic_vector(g_STREAM_WIDTH_ARRAY'range);
  -- signal stream_body_busy_array : std_logic_vector(g_STREAM_WIDTH_ARRAY'range);
  signal stream_body_ce_array : std_logic_vector(g_STREAM_WIDTH_ARRAY'range);

  signal hdebug : std_logic := '0';

  signal sys_hdr : std_logic_vector(i_sys_hdr'range);

  signal event_id : unsigned(31 downto 0);

  signal hdr_nempty               : std_logic;
  signal stream_hdr_nempty_array  : std_logic_vector(0 to g_STREAM_WIDTH_ARRAY'length-1);
  signal stream_body_nempty_array : std_logic_vector(0 to g_STREAM_WIDTH_ARRAY'length-1);

  signal data_nodes_ready : std_logic := '0';

  signal bcid_cnt : unsigned(11 downto 0);

begin

  stream_user_header_emurray_order : for ii in i_stream_user_header_emurray'range generate
    i_stream_user_header_emurray_alias(ii) <= i_stream_user_header_emurray(ii);
  end generate stream_user_header_emurray_order;

  stream_payload_emurray_order : for ii in i_stream_payload_emurray'range generate
    i_stream_payload_emurray_alias(ii) <= i_stream_payload_emurray(ii);
  end generate stream_payload_emurray_order;

  stream_hdr_payload_emurray_order : for ii in o_stream_hdr_payload_emurray'range generate
    o_stream_hdr_payload_emurray(ii) <= o_stream_hdr_payload_emurray_alias(ii);
  end generate stream_hdr_payload_emurray_order;

  stream_body_payload_emurray_order : for ii in o_stream_body_payload_emurray'range generate
    o_stream_body_payload_emurray(ii) <= o_stream_body_payload_emurray_alias(ii);
  end generate stream_body_payload_emurray_order;

  o_nempty_array(0) <= hdr_nempty;
  ROUTE_NEMPTY : for kk in 1 to g_STREAM_WIDTH_ARRAY'length generate
    o_nempty_array(2*kk-1) <= stream_hdr_nempty_array(kk-1);
    o_nempty_array(2*kk)   <= stream_body_nempty_array(kk-1);
  end generate ROUTE_NEMPTY;

  -- all data ready means that the window has passed and now it is time to read
  -- nodes out, that is why it is tied up to the packet builder enable
  -- signal. The packet builder see all the FIFO like signals from the nodes but
  -- it does know know when data is ready to be retrieved.
  o_data_ready <= all_data_ready;
  o_nodes_empty <= all_nodes_empty;

  -- head node ----------------------------------------------------------------
  --* Head node will store common information of the final packet
  u_head_node : entity daq_core.daq_head_node
    generic map(g_NBITS_SNAPSHOT => g_NBITS_SNAPSHOT,
                g_FORMAT_VERSION => g_FORMAT_VERSION,
                g_ID             => g_ID            )
    port map (i_sys_clk_fast => i_sys_clk_fast, -- : in std_logic;
              i_sys_rst      => i_sys_rst,    -- : in std_logic;
              ------------------------------------------------------------------
              i_use_snapshot => use_snapshot,
              ------------------------------------------------------------------
              i_wm_active        => wm_active,      --: in  std_logic := '0';
              i_wm_timed_out     => wm_timed_out,   --: in  std_logic := '0';
              o_data_ready       => o_hdr_data_ready, --: out std_logic := '0';
              o_error            => hnode_error,            --: out std_logic := '0';
              i_read_mode        => all_data_ready,
              i_data_nodes_ready => data_nodes_ready,
              ------------------------------------------------------------------
              i_bcid_cnt   => bcid_cnt,
              i_sys_hdr    => sys_hdr,    -- : in std_logic_vector;
              i_event_id   => event_id,   -- : in unsigned(31 downto 0);
              i_curr_bcid  => curr_bcid,
              i_wm_opening => wm_opening, -- : in unsigned(11 downto 0) := (others => '0');
              i_wm_request => wm_request, -- : in unsigned(11 downto 0) := (others => '0');
              i_wm_closing => wm_closing, -- : in unsigned(11 downto 0) := (others => '0');
              i_wm_timeout => wm_timeout,
              i_win_opening_offset => win_opening_offset, -- in unsigned(11 downto 0);
              i_win_request_offset => win_request_offset, -- in unsigned(11 downto 0);
              i_win_closing_offset => win_closing_offset, -- in unsigned(11 downto 0);
              i_win_window_timeout => win_window_timeout, -- in unsigned(11 downto 0);
              i_win_busy_threshold => win_busy_threshold, -- in unsigned(11 downto 0);
              ------------------------------------------------------------------
              i_usr_hdr_payload => i_usr_hdr_payload, -- : in std_logic_vector := "";
              ------------------------------------------------------------------
              i_full   => i_hdr_full,
              o_nempty  => o_hdr_nempty,
              o_wr_strb => o_hdr_wr_strb,  -- : in  std_logic;
              o_payload => o_hdr_payload); -- : out std_logic_vector(hdr_vt'range));

  -- data node ----------------------------------------------------------------
  --* One data node is generated per stream to match and store data.
  GEN_DATA_NODES: for jj in  0 to STREAMS-1 generate    
  begin

    u_data_node : entity daq_core.daq_data_node
      generic map (
        g_FIFO_DEPTH                  => g_FIFO_DEPTH_ARRAY(jj),
        g_FIFO_MEMORY_TYPE            => g_FIFO_MEMORY_TYPE_ARRAY(jj),
        g_NBITS_STREAM_HDR_FIFO_DEPTH => g_NBITS_STREAM_HDR_FIFO_DEPTH_ARRAY(jj),
        g_NBITS_STREAM_HDR_WIDTH      => g_NBITS_STREAM_HDR_WIDTH_ARRAY(jj),
        g_NBITS_STREAM_HDR_COUNTER    => g_NBITS_STREAM_HDR_COUNTER_ARRAY(jj),
        g_NBITS_STREAM_HDR_USER       => g_NBITS_STREAM_HDR_USER_ARRAY(jj),
        g_GHDL                        => g_GHDL
      )
      --------------------------------------------------------------------------
      port map (
        i_sys_clk_fast => i_sys_clk_fast, -- : in std_logic;
        i_sys_rst      => i_sys_rst,    -- : in std_logic;
        ----------------------------------------------------------------
        i_wm_active    => wm_active, --: in  std_logic;
        i_wm_timed_out => wm_timed_out, --: in  std_logic;
        i_wm_opening   => wm_opening, --: in  unsigned(11 downto 0);
        i_wm_closing   => wm_closing, --: in  unsigned(11 downto 0);
        o_error        => dnodes_error(jj), --: out std_logic;
        i_read_mode    => all_data_ready,
        ----------------------------------------------------------------
        i_stream_user_header => i_stream_user_header_emurray_alias(high(g_NBITS_STREAM_HDR_USER_ARRAY, jj) downto low(g_NBITS_STREAM_HDR_USER_ARRAY, jj)),
        i_stream_ctrl_bcid   => i_stream_ctrl_bcid_array(i_stream_ctrl_bcid_array'low+jj), --: in  unsigned(11 downto 0);
        i_stream_data_bcid   => i_stream_data_bcid_array(i_stream_data_bcid_array'low+jj), -- : in unsigned(11 downto 0);
        i_stream_valid       => i_stream_valid_array(i_stream_valid_array'low+jj),   -- : in std_logic;
        i_stream_enable      => i_stream_enable_array(i_stream_enable_array'low+jj),     -- : in std_logic;
        i_stream_payload     => i_stream_payload_emurray_alias(high(g_STREAM_WIDTH_ARRAY, jj) downto low(g_STREAM_WIDTH_ARRAY, jj)),
        ----------------------------------------------------------------
        i_hdr_full       => i_stream_hdr_full_array(jj),
        o_hdr_nempty     => o_stream_hdr_nempty_array(jj),
        o_hdr_wr_strb    => o_stream_hdr_wr_strb_array(jj),
        o_hdr_data_ready => o_stream_hdr_data_ready_array(jj),
        o_hdr_payload    => o_stream_hdr_payload_emurray_alias(high(STREAM_HDR_WIDTH_ARRAY, jj) downto low(STREAM_HDR_WIDTH_ARRAY, jj)),
        ----------------------------------------------------------------
        i_body_full       => i_stream_body_full_array(jj),
        o_body_nempty     => o_stream_body_nempty_array(jj),
        o_body_wr_strb    => o_stream_body_wr_strb_array(jj), -- : in  std_logic;
        o_body_data_ready => o_stream_body_data_ready_array(jj), -- : in  std_logic;
        o_body_payload    => o_stream_body_payload_emurray_alias(high(g_STREAM_WIDTH_ARRAY, jj) downto low(g_STREAM_WIDTH_ARRAY, jj))
      );
  end generate GEN_DATA_NODES;



  
  all_data_ready <= '1' when (o_hdr_data_ready = '1'
                              and o_stream_hdr_data_ready_array = (o_stream_hdr_data_ready_array'range => '1')
                              and o_stream_body_data_ready_array = (o_stream_body_data_ready_array'range => '1'))
                    else '0';
 
  data_nodes_ready <= '1' when (o_stream_hdr_data_ready_array = (o_stream_hdr_data_ready_array'range => '1')
                                and o_stream_body_data_ready_array = (o_stream_body_data_ready_array'range => '1'))
                      else '0';

  all_nodes_empty <= '1' when (o_hdr_nempty = '0'
                               and o_stream_hdr_nempty_array = (o_stream_hdr_nempty_array'range => '0')
                               and o_stream_body_nempty_array = (o_stream_body_nempty_array'range => '0'))
                     else '0';
  
  
  -- process (i_sys_clk_fast)
  -- begin
  --   if rising_edge(i_sys_clk_fast) then
  --     all_data_ready <= '0';
  --     if (o_hdr_data_ready = '1'
  --         and o_stream_hdr_data_ready_array = (o_stream_hdr_data_ready_array'range => '1')
  --         and o_stream_body_data_ready_array = (o_stream_body_data_ready_array'range => '1')) then
  --       all_data_ready <= '1';
  --     end if;
  --     data_nodes_ready <= '1' when o_stream_body_data_ready_array = (o_stream_body_data_ready_array'range => '1') else '0';
  --   end if;
  -- end process;

  --# --* all_nodes_empty is the flag that the FSM needs to understand when all
  --# --* nodes  are empty to release the window matching engine.
  --# PROC_ALL_NODES_EMPTY : process (i_sys_clk_fast)
  --# begin
  --#   if rising_edge(i_sys_clk_fast) then
  --#     if (o_hdr_nempty = '0'
  --#         and o_stream_hdr_nempty_array = (o_stream_hdr_nempty_array'range => '0')
  --#         and o_stream_body_nempty_array = (o_stream_body_nempty_array'range => '0')) then
  --#       all_nodes_empty <= '1';
  --#     else
  --#       all_nodes_empty <= '0';
  --#     end if;
  --#   end if;
  --# end process PROC_ALL_NODES_EMPTY;

  o_wm_active <= wm_active or i_req_strb;

  --* FSM to coordenate the activity happening in the window matching
  --* engine
  PROC_FSM : process (i_sys_clk_fast)
  begin
    if rising_edge(i_sys_clk_fast) then

      if i_sys_rst = '1' then
        state <= idle;

      else

        case state is

          -- while idle, window matching engine should stay inactive waiting
          -- for a write strobe caused by a data request. Once the write
          -- strobe arrives, it should register the window info used by the
          -- nodes, as well as other parameters used in the header.
          when IDLE =>

            wm_active <= '0';

            if  i_req_strb = '1' then

              wm_active <= '1';

              use_snapshot <= i_use_snapshot;

              --* all the nodes share window information, it is registered just
              --* once here
              bcid_cnt           <= i_bcid_cnt;
              sys_hdr            <= i_sys_hdr;
              event_id           <= i_event_id;
              curr_bcid          <= i_curr_bcid;
              wm_opening         <= i_win_opening_cnt;
              wm_request         <= i_win_request_cnt;
              wm_closing         <= i_win_closing_cnt;
              wm_timeout         <= i_win_window_timeout;
              win_opening_offset <= i_win_opening_offset;
              win_request_offset <= i_win_request_offset;
              win_closing_offset <= i_win_closing_offset;
              win_window_timeout <= i_win_window_timeout;
              win_busy_threshold <= i_win_busy_threshold;

              wm_timed_out <= '0';

              state <= COLLECT;

            end if;

          -- Collect state exists to follow the behavior of the nodes while
          -- taking data, and to force the end of the data gathering in case
          -- timeout is reached.
          when COLLECT =>

            if i_sys_bx = '1' then
              if wm_timeout > 0 then
                wm_timeout <= wm_timeout - 1;
              else
                wm_timed_out <= '1';
              end if;
            end if;

            if all_data_ready = '1' then
              state <= SEND;
            end if;

          -- the engine will sit on the send state until all data is retrieved
          -- by the packet builder.
          when SEND =>
            if all_nodes_empty = '1' then
              wm_active <= '0';
              state <= idle;
            end if;
        end case;

      end if; -- reset
    end if; -- clk
  end process PROC_FSM;


end architecture V2;
