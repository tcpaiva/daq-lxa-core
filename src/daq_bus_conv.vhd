library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library daq_core;

--* @author Thiago Costa de Paiva
--* @id $git$
--* @version $git$
--*
--* @brief The bus converter module adapts the variable width of the streams to
--* the constant width of the packet builders
entity daq_bus_conv is
  generic (
    --* Depth of the internal FIFO
    g_FIFO_DEPTH : integer;
    --* Type of the internal FIFO: "auto", "bram", "uram", "distributed"
    g_FIFO_TYPE  : string;
    g_GHDL       : boolean
  );
  port (
    --* fast clock
    i_sys_clk_fast : in std_logic;
    --* system reset
    i_sys_rst      : in std_logic;
    ------------------------------------------------------------------------
    --* flag indicating that data in all nodes is ready to be retrieved
    i_all_data_ready  : in  std_logic;
    i_all_nodes_empty : in  std_logic;
    --* flag indicating that all bus converters have finished sending data
    i_all_bconv_ready  : in  std_logic;
    i_all_bconv_done  : in  std_logic;
    --* flag indicating that this bus converter have finished sending data
    o_bconv_done      : out std_logic;
    o_bconv_ready     : out std_logic;
    ------------------------------------------------------------------------
    --* data coming from a node
    i_src_payload      :  in std_logic_vector;
    --* write strobe coming from a node
    i_src_wr_strb      :  in std_logic;
    --* not empty flag coming from a node
    i_src_nempty       :  in std_logic;
    i_src_data_ready   :  in std_logic;
    --* full flag to a node indicating that data cannot be received
    o_src_full         : out std_logic;
    ------------------------------------------------------------------------
    --* enable flag from packet builder allowing data to be written out
    i_dst_ce       :  in std_logic;
    --* flag indicating that packet builder cannot receive data
    i_dst_pfull    :  in std_logic;
    --* write strobe to packet builder internal FIFO
    o_dst_wr_strb  : out std_logic;
    --* data payload to packet builder internal FIFO 
    o_dst_payload  : out std_logic_vector;
    --* not empty flag to the packet builder
    o_dst_nempty   : out std_logic
  );
end entity daq_bus_conv;

architecture V2 of daq_bus_conv is

  --* Get the width of the wider bus
  function get_wide_width (a, b: std_logic_vector) return natural is
  begin
    if a'length >= b'length then
      return a'length;
    else -- i_src_payload'length-1 < o_dst_payload'length
      return b'length;
    end if;
  end function get_wide_width;
  --* Width of the wider bus
  constant WIDE_WIDTH : natural := get_wide_width(i_src_payload, o_dst_payload);
  
  --* Get the width of the narrower bus
  function get_narrow_width (a, b: std_logic_vector) return natural is
  begin
    if a'length >= b'length then
      return b'length;
    else -- i_src_payload'length < o_dst_payload'length
      return a'length;
    end if;
  end function get_narrow_width;
  --* width of the narrower bus
  constant NARROW_WIDTH : natural := get_narrow_width(i_src_payload, o_dst_payload);
  
  --* Calculate K as the number of words needed to convert between buses
  function get_k (src, dst: std_logic_vector) return natural is
    variable extra_slice : natural := 0;
    variable y : natural := 0;
  begin
    -- if the input bus is narrower than the output bus, we should be able to
    -- include __at most__ floor(OUTPUT_WIDTH/INPUT_WIDTH) words in the output
    -- bus, therefore y is already correct. However, if the input bus is wider
    -- than the output bus, we need to allocate one extra slice to deal with the
    -- last bits of the input word sent through the output bus.
    if (src'length > dst'length) and (src'length mod dst'length /= 0) then
      extra_slice := 1;
    end if;

    if src'length >= dst'length then 
      y := (src'length / dst'length) + extra_slice;
    else
     y := (dst'length / src'length) + extra_slice;
    end if;
    return y;
  end function get_k;
  --* number of words needed to convert between buses
  constant K : natural := get_k(i_src_payload, o_dst_payload);

  -- index to map words between buses. We send first the left most slice.
  signal   index : natural := K-1;

  -- We need to ensure that the width of the wide bus is a multiple of the width
  -- of the narrow bus, so it can be multiplexed without much logic penalty
  -- (yes, it means that we are wasting communication bandwidth, mainly when the
  -- widths are almost the same; this can be optimized later if needed). Let's
  -- use an auxiliary bus with the adjusted width.
  signal aux_bus : std_logic_vector((K*NARROW_WIDTH)-1 downto 0) := (others => '0');

  -- auxiliary bus in slices
  type slices_t is array(K-1 downto 0) of std_logic_vector(NARROW_WIDTH-1 downto 0);
  signal slices : slices_t := (others => (others => '0'));


  --* IDLE: bus converter available 
  --*
  --* DATA_READY: all nodes are ready to be read out
  --*
  --* WAITING: check if there is data to be sent out
  --*
  --* NEW_WORD: read a word to be divided and sent out
  --*
  --* SENDING: send a word out
  --*
  --* HALT: wait for all the bus converters to finish (synchronization)
  type state_t is (IDLE,
                   WAIT_TRANSFER_FROM_WME,
                   WAIT_OTHER_BCONVS_READY,
                   PREPARE_TRANSFER_TO_PBLDR,
                   TRANSFER_TO_PBLDR,
                   WAIT_OTHER_BCONVS_DONE,
                   HALT);
  signal state : state_t := IDLE;

  signal dst_nempty : std_logic := '0';
  signal nempty     : std_logic := '0';
  signal nempty_aux : std_logic := '0';

  signal src_full : std_logic := '0';

  signal fifo_input_full    : std_logic;
  signal fifo_output_rd_strb : std_logic;
  signal  fifo_output_nempty : std_logic;
  signal fifo_output_payload : std_logic_vector(i_src_payload'range);

  signal busy : std_logic := '0';

begin

  WIDE_TO_NARROW: if i_src_payload'length >= o_dst_payload'length generate
    signal dummy : unsigned(-1 downto 0);
  begin

    o_src_full <= '1' when fifo_input_full = '1' or src_full = '1' else '0';
    
    -- o_dst_nempty <= '1' when nempty = '1' or dst_nempty = '1' else '0';



    --* FIFO to store the data to be converted
    u_fifo : entity daq_core.daq_fifo
    generic map (g_FIFO_DEPTH       => g_FIFO_DEPTH,
                 g_FIFO_MEMORY_TYPE => g_FIFO_TYPE,
                 g_GHDL             => g_GHDL)
    port map (i_sys_clk_fast =>      i_sys_clk_fast,
              i_sys_rst      =>           i_sys_rst,
              --------------------------------------------------
              o_src_full    =>  fifo_input_full,
              i_src_payload  =>  i_src_payload,
              i_src_wr_strb  =>  i_src_wr_strb,
              --------------------------------------------------
              i_dst_rd_strb  => fifo_output_rd_strb,
              o_dst_nempty   =>  fifo_output_nempty,
              o_dst_payload  => fifo_output_payload,
              o_dst_wr_cnt   => dummy);



    
    -- bus width conversion is done via a multiplexer that routes the correct
    -- piece of the input word to the output bus. The next lines connect the
    -- input bus to all the input words of the multiplexer.
    fg: for i in 0 to K-1 generate
      slices(i) <= aux_bus((i+1)*NARROW_WIDTH-1 downto i*NARROW_WIDTH);
    end generate fg;
    
    -- Finally, route each slice of the source word to the destination word
    o_dst_payload(NARROW_WIDTH-1 downto 0) <= slices(index);

    -- nempty signal must be on from the very moment that the wr_strb happen
    -- preventing the delay caused by the FIFO
    PROC_NEMPTY_AUX : process(i_sys_clk_fast)
    begin
      if rising_edge(i_sys_clk_fast) then
        if i_src_wr_strb = '1' then
          nempty_aux <= '1';
        elsif fifo_output_nempty = '1' then
          nempty_aux <= '0';
        end if;
      end if;
    end process PROC_NEMPTY_AUX;
    nempty <= '1' when i_src_wr_strb = '1' or nempty_aux = '1' or fifo_output_nempty = '1' else '0';

    -- bconv is nempty while fifo has data and data is transferred
    o_dst_nempty <= '1' when nempty = '1' or dst_nempty = '1' else '0';
    
    --* Coordination of activities in the bus converter
    PROC_FSM: process (i_sys_clk_fast)
    begin

      if rising_edge(i_sys_clk_fast) then
      
        if i_sys_rst = '1' then
          state <= IDLE;
          busy <= '0';
          dst_nempty <= '0';
          o_bconv_ready       <= '0';
          o_bconv_done        <= '0';
        else

          -- safe default
          o_dst_wr_strb       <= '0';
          fifo_output_rd_strb <= '0';
          src_full            <= '1';
    
          case state is

            when IDLE =>
              dst_nempty <= '0';
              o_bconv_ready       <= '0';
              o_bconv_done        <= '0';
              if i_src_data_ready = '1' then
                src_full <= '0';
                state <= WAIT_TRANSFER_FROM_WME;
              end if;

            when WAIT_TRANSFER_FROM_WME =>
              src_full <= '0';
              -- if i_all_nodes_empty = '1' then
              -- if i_src_nempty = '0' then
              if i_src_data_ready = '0' then
                src_full <= '1';
                o_bconv_ready <= '1';
                state <= WAIT_OTHER_BCONVS_READY;
              end if;              

            when WAIT_OTHER_BCONVS_READY =>
              if i_all_bconv_ready = '1' then
                state <= PREPARE_TRANSFER_TO_PBLDR;
              end if;
              
            when PREPARE_TRANSFER_TO_PBLDR =>
              index <= K-1;
              aux_bus(aux_bus'high downto aux_bus'high-fifo_output_payload'length+1) <= fifo_output_payload;

              if nempty = '1' then
                dst_nempty <= '1';
                if fifo_output_nempty = '1' and i_dst_pfull = '0' and i_dst_ce = '1' then
                  fifo_output_rd_strb <= '1';
                  o_dst_wr_strb <= '1';
                  state <= TRANSFER_TO_PBLDR;
                end if;
              else
                -- o_bconv_ready <= '0';
                o_bconv_done <= '1';
                state <= WAIT_OTHER_BCONVS_DONE;
              end if;

            when TRANSFER_TO_PBLDR =>

              -- sending slices of current word
              if i_dst_pfull = '0' and i_dst_ce = '1' then
                
                index <= index - 1 when K > 1 else index;
                o_dst_wr_strb <= '1' when K > 1 else '0';
                
                -- current word is finished
                if index <= 1 then
                  dst_nempty <= '0';
                  state <= PREPARE_TRANSFER_TO_PBLDR;
                end if;
                
              end if;

            when WAIT_OTHER_BCONVS_DONE =>
              if i_all_bconv_done = '1' then
                state <= IDLE;
              end if;
              
            when HALT =>
              -- if i_dst_ce = '0' then
              if i_src_data_ready = '0' then
                state <= IDLE;
                o_bconv_ready       <= '0';
                o_bconv_done        <= '0';
              end if;

          end case;

        end if; -- rst
      end if; -- clk
    end process PROC_FSM;    

  end generate WIDE_TO_NARROW;

  SAME_WIDTH: if i_src_payload'length = o_dst_payload'length generate

    -- Tue 16 Aug 2022 06:49:19 AM EDT
    -- -- if input and output bus width are the same, no conversion is needed.
    -- o_dst_payload    <= i_src_payload;
    -- o_dst_nempty  <= i_src_nempty;
    -- o_src_rd_strb <=  i_dst_rd_strb;

  end generate SAME_WIDTH;

  NARROW_TO_WIDE: if i_src_payload'length < o_dst_payload'length generate

    --Tue 16 Aug 2022 06:49:19 AM EDT
    -- -- mapping the auxiliary bus to the output bus
    -- o_dst_payload(aux_bus'range) <= aux_bus;
    -- 
    -- 
    -- -- bus width conversion is done via a multiplexer that routes the input bus
    -- -- to the correct slice of the output bus. The next lines connect bus slices
    -- -- to the auxiliary bus.
    -- fg: for i in 0 to K-1 generate
    --   aux_bus((i+1)*NARROW_WIDTH-1 downto i*NARROW_WIDTH) <= slices(i);
    -- end generate fg;
    -- 
    -- -- Finally, route the input bus to each slice of the auxiliary bus if source
    -- -- is not empty. Route a zeroed word otherwise to clear remaining output
    -- -- bits.
    -- slices(index) <= i_src_payload(NARROW_WIDTH-1 downto 0)
    --                  when i_src_nempty = '1'
    --                  else (others => '0');
    -- 
    -- -- nempty signal will be active when the last slice of the auxiliary bus is
    -- -- received and packet builder did not read it. If K=1, index is always 0,
    -- -- therefore read strobe from packet builder should bypass to src.
    -- o_dst_nempty <=
    --   '1' when (index = 1
    --             or (K>1 and index = 0 and i_dst_rd_strb = '0')
    --             or (K=1 and i_dst_rd_strb = '1'))
    --   else '0';
    -- 
    -- -- handling indexes of the auxiliary slices connected to the input bus.
    -- process (i_sys_clk_fast)
    -- begin
    --   if rising_edge(i_sys_clk_fast) then
    --     if i_sys_rst = '1' then
    --       index <= K-1;
    --     else
    -- 
    --       -- whenever the destination sends a read strobe, restart index
    --       if i_dst_rd_strb = '1' then
    --         index <= k-1;
    -- 
    --       -- otherwise, decrement index down to 0, then it waits for read strobe
    --       -- from destination.
    --       else
    --         index <= index-1 when index > 0 else index;
    --       end if;
    -- 
    --     end if;
    --   end if;
    -- end process;
    -- 
    -- 
    -- -- K=1 => index always 0;
    -- -- src receive read strobe when
    -- -- * src is not empty and there are slices to fill
    -- -- * src is not empty, one slice only, and a read strobe is received
    -- o_src_rd_strb <= '1' when (i_src_nempty = '1'
    --                            and ((K > 1 and index > 0)
    --                                 or (K=1 and i_dst_rd_strb = '1')))
    --                  else '0';

  end generate NARROW_TO_WIDE;

end V2;
