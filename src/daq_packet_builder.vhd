library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library daq_core;
use daq_core.yml2hdl.all;
use daq_core.daq_v3.all;
use daq_core.daq_defs.all;
use daq_core.yml2hdl.all;


--* @author Thiago Costa de Paiva
--* @id $git$
--* @version $git$
--*
--* @brief Packet builder will activate whenever all nodes indicate they are
--* finished with collecting data. When enabled, it will sequentially retrieve
--* data in sequence from each node related to a given window matching engine.
--* The packet is assembled and then stored in a First-Word-Falls-Through FIFO
--* to make full use of the capacity of the window matching engines,
--* from were it is sent out. 

entity daq_packet_builder is
  generic (
    --* Array with widths of the header of each stream
    g_STREAM_HDR_WIDTH_ARRAY : integer_vector;
    --* Array with widths of the payload of each stream
    g_STREAM_WIDTH_ARRAY     : integer_vector;
    --* Depth of the FIFO 
    g_FIFO_DEPTH             :        integer;
    --* Type of the FIFO: "auto", "bram", "uram" (Xilinx family dependent), "distributed"
    g_FIFO_TYPE              : string;
    g_GHDL                   : boolean
  );
  port (
    --* fast clock
    i_sys_clk_fast     : in std_logic;
    --* system reset
    i_sys_rst          : in std_logic;
    -- control and monitoring ========================================
    --* flag indicating that the assembling of a packet is happening
    o_active           : out std_logic;
    -- nodes =========================================================
    --* flag indicating that the packet builder cannot receive words 
    o_pfull_array      : out             std_logic_vector;
    --* enable flag to each bus converter
    o_ce_array         : out             std_logic_vector;
    --* flag indicating that bus converter is not empty
    i_nempty_array     :  in             std_logic_vector;
    --* FIFO write strobe from each bus converter
    i_wr_strb_array    :  in             std_logic_vector;
    --* FIFO data payload from each bus converter
    i_payload_array    :  in       std_logic_vector_array;
    -- ===============================================================
    i_all_bconv_done  : in std_logic;
    i_all_bconv_ready : in std_logic;
    -- f2e ===========================================================
    --* flag indicating that words cannot be written to the output
    i_f2e_pfull        :  in                    std_logic;
    --* write strobe to write data words to the output
    o_f2e_wr_strb      : out                    std_logic;
    --* output data payload
    o_f2e_payload      : out             std_logic_vector;
    --* control bits indicating the start and end of a packet
    o_f2e_ctrl         : out std_logic_vector(1 downto 0)
  );

end entity daq_packet_builder;

architecture V2 of daq_packet_builder is

  --* Shortcut to get the width of the output payload
  constant OUTPUT_WIDTH : integer := o_f2e_payload'length;

  -- node selection
  signal node_fifo_sel : natural := 0;
  signal fifo_nxt : natural := 1;

  --* Control bits to start a packet. Corresponding data word should be discarded.
  constant START_OF_PACKET : std_logic_vector := "10";
  --* Control bits to end a packet. Corresponding data word should be discarded.
  constant END_OF_PACKET : std_logic_vector := "01";
  --* Control bits for a valid word in a packet.
  constant VALID_WORD : std_logic_vector := "00";
  --* Control bits for a valid word in a packet.
  constant INVALID_WORD : std_logic_vector := "11";

  signal f2e_wr_strb_aux : std_logic;

  -- whatch if data is available on any node, which should be whenever a daq
  -- request is received.
  signal any_nempty : std_logic := '0';

  -- CRC sent as trailer
  signal crc : std_logic_vector(OUTPUT_WIDTH-1 downto 0) := (others => '0');

  --* Calculates a simple CRC for a given packet. Each word is bit-wise XORed
  --* with the previous resultant CRC. CRC is initiated with zeros.
  function xor_crc (x, curr_crc: std_logic_vector) return std_logic_vector is
    variable y: std_logic_vector(x'range);
  begin
    for j in x'range loop
      y(j) := x(j) xor curr_crc(j);
    end loop;
    return y;
  end function xor_crc;

  signal busy : std_logic := '0';

  signal f2e_payload_d1 : std_logic_vector(o_f2e_payload'range);
  signal wr_strb_array_d1 : std_logic;

  signal    fifo_input_pfull : std_logic;
  signal  fifo_input_payload : std_logic_vector(o_f2e_payload'length+1 downto 0);
  signal  fifo_input_wr_strb : std_logic;

  signal fifo_output_rd_strb_aux : std_logic := '0';
  signal fifo_output_rd_strb     : std_logic := '0';
  signal fifo_output_nempty      : std_logic;
  signal fifo_output_payload     : std_logic_vector(o_f2e_ctrl'length+o_f2e_payload'length-1 downto 0);

  signal rd_strb : std_logic := '0';

  --* IDLE: builder is available to start assembling a packet.
  --* 
  --* STORE_STA: all data in bus converters are ready to be retrieved
  --* 
  --* STORE_DATA: data from bus converters are retrieved in sequence.
  --* 
  --* STORE_STO: after content of the packet is ready, CRC and end of packet
  --* are inserted before returning to IDLE state.
  type src_state_t is (IDLE,
                       STORE_DATA,
                       STORE_CRC,
                       STORE_STO);
  signal src_state : src_state_t := IDLE;

  signal fifo_output_nempty_d : std_logic;

  signal nempty_array_d : std_logic_vector(i_nempty_array'range);

  signal fifo_input_pfull_d : std_logic;

  signal dummy : unsigned(0 downto 1);

  signal f2e_pfull_d : std_logic := '1';

  signal f2e_output : std_logic_vector(fifo_output_payload'range);
  
begin

  --* FIFO to store assembled packets. Depth and type are parameterized.
  u_fifo : entity daq_core.daq_fifo
    generic map (g_FIFO_DEPTH       => g_FIFO_DEPTH,
                 g_FIFO_MEMORY_TYPE => g_FIFO_TYPE,
                 g_GHDL             => g_GHDL)
    port map (i_sys_clk_fast =>      i_sys_clk_fast,
              i_sys_rst      =>           i_sys_rst,
              --------------------------------------------------
              o_src_pfull    =>    fifo_input_pfull,
              i_src_payload  =>  fifo_input_payload,
              i_src_wr_strb  =>  fifo_input_wr_strb,
              --------------------------------------------------
              i_dst_rd_strb  => fifo_output_rd_strb,
              o_dst_nempty   =>  fifo_output_nempty,
              o_dst_payload  => fifo_output_payload,
              o_dst_wr_cnt   => dummy);


  -- 2 steps needed to overcome VHDL-2008 limitations of Vivado
  f2e_output <=   fifo_output_payload when fifo_output_nempty = '1' else INVALID_WORD & (o_f2e_payload'range => '0');
  (o_f2e_ctrl, o_f2e_payload) <= f2e_output;


  fifo_output_rd_strb <= '1' when fifo_output_nempty = '1' and f2e_pfull_d = '0' else '0';
  o_f2e_wr_strb       <= fifo_output_rd_strb;

  GY : for ii in o_pfull_array'range generate
    o_pfull_array(ii) <= fifo_input_pfull when node_fifo_sel = ii and busy = '0' else '1';
  end generate GY;

  GX : for ii in o_ce_array'range generate
    o_ce_array(ii) <= '1' when node_fifo_sel = ii and o_active = '1' else '0';
  end generate GX;

  --* Process to retrieve words from bus converters in sequence (de-randomizer)
  --* until each bus converter FIFO is empty. Before the content of the packet,
  --* a start word is inserted and after the content, both CRC and end word are
  --* included. This way, a full packet is stored in the FIFO.
  PROC_BUILD_PACKET : process (i_sys_clk_fast)
  begin
    if rising_edge(i_sys_clk_fast) then

      f2e_pfull_d <= i_f2e_pfull;
      
      fifo_input_pfull_d <= fifo_input_pfull;

      if i_sys_rst = '1' then
        src_state     <= IDLE;

      else

        -- safe defaults
        fifo_input_wr_strb  <=  '0';
        busy <= '1';

        case src_state is
          when IDLE =>
            fifo_input_payload <= (fifo_input_payload'range => '0');
            node_fifo_sel <= 0;
            o_active <= '0';
            crc <= (crc'range => '0');
            if i_all_bconv_ready = '1' and fifo_input_pfull = '0' then
              o_active <= '1';
              fifo_input_wr_strb <= '1';
              fifo_input_payload <= START_OF_PACKET & (o_f2e_payload'range => '0');
              busy <= '0';
              src_state <= STORE_DATA;
            end if;
            
          when STORE_DATA =>
            busy <= '0';
            if i_wr_strb_array(node_fifo_sel) = '1' then
              fifo_input_payload <= VALID_WORD & i_payload_array(node_fifo_sel);
              fifo_input_wr_strb <= '1';
              crc <= xor_crc(i_payload_array(node_fifo_sel), crc);
            elsif i_nempty_array(node_fifo_sel) = '0' then
              busy <= '1';                
              if node_fifo_sel < (2 * g_STREAM_WIDTH_ARRAY'length) then
                node_fifo_sel <=  node_fifo_sel + 1;
              else
                -- o_active <= '0';
                src_state <= STORE_CRC;
              end if;
            end if;

          -- state required to avoid transmission of buffered bus conv data
          when STORE_CRC =>
            if fifo_input_pfull = '0' then
              fifo_input_payload <= VALID_WORD & crc;
              fifo_input_wr_strb <= '1';
              src_state <= STORE_STO;
            end if;
            
          when STORE_STO =>
            fifo_input_payload <= END_OF_PACKET & (o_f2e_payload'range => '0');
            if fifo_input_pfull = '0' then
              fifo_input_wr_strb <= '1';
              src_state <= IDLE;
            end if;

        end case;

      end if; -- sys_rst

    end if; -- clk

  end process PROC_BUILD_PACKET;

  --) process (i_sys_clk_fast)
  --) begin
  --)   if rising_edge(i_sys_clk_fast) then
  --)     if fifo_input_wr_strb <= '1' then
  --)       crc <= xor_crc(i_payload_array(node_fifo_sel), crc);
  --)     end if;
  --)   end if;
  --) end process;
  
end V2;
