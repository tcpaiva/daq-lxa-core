library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library daq_core;
use daq_core.daq_v3.all;
use daq_core.daq_defs.all;
use daq_core.yml2hdl.all;

--* @author Thiago Costa de Paiva
--* @id $git$
--* @version $git$

--* @brief Structures the implementation instantiating window matching engines,
--* packet builders, and the pointer handler. It is in this block that all the
--* multiplexing between window matching engines and bus converters happens.
--*
--* Each algorithm block consists of the input streams and multiple output
--* buses. To match the widths between input and output buses, there are bus
--* converter blocks. Each stream has its own payload width, but all the output
--* buses are configured with the same width.
--*
--* The stream interface is simple: a payload, an ID counter to mark the
--* payload information and a write strobe to validate that payload. Each output
--* interface consists of a payload bit-vector, control bits to indicate the
--* begining and end of packets, and a validation bit.
--*
--* Upon receiving a request, a window matching engine will be activated. Every
--* window matching engine activation happens in sequence. Multiple matching
--* window engines can be used in parallel. As data related to a request must
--* be sent in the same order that the requests were received, window matching
--* engines are also read sequentially.

entity daq_algo is
  generic (
    --* number of window matching engines
    g_PIPELINES                         : integer;
    --* array with the width of each stream
    g_STREAM_WIDTH_ARRAY                : integer_vector;
    --* array with the FIFO depth of each stream
    g_FIFO_DEPTH_ARRAY                  : integer_vector;
    --* array with the FIFO type for each stream
    g_FIFO_MEMORY_TYPE_ARRAY            : string_vector;
    --* number of bits used in the packet header to represent the stream FIFO
    --* depth
    g_NBITS_STREAM_HDR_FIFO_DEPTH_ARRAY : integer_vector;
    --* number of bits used in the packet header to represent the stream width
    g_NBITS_STREAM_HDR_WIDTH_ARRAY      : integer_vector;
    --* number of bits used in the packet header to represent the stream FIFO
    --* word counter
    g_NBITS_STREAM_HDR_COUNTER_ARRAY    : integer_vector;
    --* number of bits used in the packet header to represent the stream user
    --* defined field
    g_NBITS_STREAM_HDR_USER_ARRAY       : integer_vector;
    --* number of bits used in the packet header to represent the window
    --* matching engines status
    g_NBITS_SNAPSHOT                    : integer :=  16;
    --* depth of the FIFO used in the packet builders
    g_PACKET_BUILDER_FIFO_DEPTH         : integer := 512;
    --* type of the FIFO used in the packet builders
    g_PACKET_BUILDER_FIFO_TYPE          : string := "auto";
    --* format version number
    g_FORMAT_VERSION                    : natural;
    --* type of the fifo used in the bus converter: "auto", "bram", "uram",
    --* "distributed"
    g_BUS_CONVERTER_FIFO_TYPE           : string := "auto";
    g_GHDL                              : boolean := false
  );
  -----------------------------------------------------------------------------
  port (
    --* fast clock
    i_sys_clk_fast                 : in std_logic;
    --* system reset
    i_sys_rst                      : in std_logic;
    -- id counter strobe
    i_sys_bx                       : in std_logic;
    ------------------------------------------------------------------------
    --* first bits of the packet header
    i_sys_hdr                      : in std_logic_vector;
    ------------------------------------------------------------------------
    --* flag indicating that there is at least one window matching engine
    --* available
    o_wme_available                : out std_logic;
    ------------------------------------------------------------------------
    --* request strobe
    i_req_strb                     : in std_logic;
    --* value of the calculated opening window edge to be used
    i_win_opening_cnt              : in unsigned(11 downto 0);
    --* value of the calculated request counter to be used
    i_win_request_cnt              : in unsigned(11 downto 0);
    --* value of the calculated closing window edge to be used
    i_win_closing_cnt              : in unsigned(11 downto 0);
    -- configuration -------------------------------------------------------
    --* offset used to calculate the opening window edge
    i_win_opening_offset           : in unsigned(11 downto 0);
    --* offset used to calculate the request counter edge
    i_win_request_offset           : in unsigned(11 downto 0);
    --* offset used to calculate the closing window edge
    i_win_closing_offset           : in unsigned(11 downto 0);
    --* Window timeout with respect to the ID counter frequency
    i_win_window_timeout           : in unsigned(11 downto 0);
    --* maximum number of window matching engines used before busy flag is
    --* raised
    i_win_busy_threshold               : in unsigned(07 downto 0);
    ------------------------------------------------------------------------
    --* flag indicating that the threshold of window matching engine used was
    --* reached
    o_busy                         : out std_logic;
    ------------------------------------------------------------------------
    --* id counter
    i_bcid_cnt                     : in unsigned(11 downto 0);
    ------------------------------------------------------------------------
    --* current event counter
    i_event_id                     : in unsigned(31 downto 0);
    --* id counter when request is released to the window matching engine
    i_curr_bcid                    : in unsigned(11 downto 0);
    --* user defined packet header field
    i_usr_hdr_payload              : in std_logic_vector;
    ------------------------------------------------------------------------
    --* concatenated user defined stream headers in a bit-vector
    i_stream_user_header_emurray   : in std_logic_vector;
    --* control id for each stream in an array
    i_stream_ctrl_bcid_array       : bcid_array_t;
    --* data id for each stream in an array
    i_stream_data_bcid_array       : bcid_array_t;
    --* data valid for each stream in an array
    i_stream_valid_array           : std_logic_vector;
    --* enable for each stream in an array
    i_stream_enable_array          : std_logic_vector;
    --* concatenated payload of each stream in a bit-vector
    i_stream_payload_emurray       : std_logic_vector;
    ------------------------------------------------------------------------
    --* array of flags indicating that data cannot be sent out
    i_f2e_pfull_array              : in  std_logic_vector;
    --* array of control
    o_f2e_ctrl_array               : out std_logic_vector_array;
    --* array of write strobes
    o_f2e_wr_strb_array            : out std_logic_vector;
    --* array of data payloads
    o_f2e_payload_array            : out std_logic_vector_array
  );
end entity daq_algo;

architecture V2 of daq_algo is

  --* width of the output bus
  constant OUTPUT_WIDTH : integer := o_f2e_payload_array(o_f2e_payload_array'low)'length;
  --* number of input streams
  constant STREAMS : integer := g_STREAM_WIDTH_ARRAY'length;

  --* Calculation of the stream header width for each stream
  function get_stream_hdr_width_array return integer_vector is
    variable y : integer_vector(g_NBITS_STREAM_HDR_FIFO_DEPTH_ARRAY'range);
  begin
    for j in g_STREAM_WIDTH_ARRAY'range loop
      y(j) :=  g_NBITS_STREAM_HDR_FIFO_DEPTH_ARRAY(j);
      y(j) := y(j) + g_NBITS_STREAM_HDR_WIDTH_ARRAY(j);
      y(j) := y(j) + g_NBITS_STREAM_HDR_COUNTER_ARRAY(j);
      y(j) := y(j) + g_NBITS_STREAM_HDR_USER_ARRAY(j);
    end loop;
    return y;
  end function get_stream_hdr_width_array;
  constant STREAM_HDR_WIDTH_ARRAY : integer_vector := get_stream_hdr_width_array;

  signal wm_rd_wme_ptr_vector   : integer_vector(i_f2e_pfull_array'range) := (others => 0);
  signal wm_rd_pbldr_ptr_vector : integer_vector(0 to g_PIPELINES-1) := (others => 0);

  signal wm_req_strb_array    : std_logic_vector(0 to g_PIPELINES-1) := (others => '0');
  signal wm_data_ready_array  : std_logic_vector(0 to g_PIPELINES-1) := (others => '0');
  signal wm_nodes_empty_array : std_logic_vector(0 to g_PIPELINES-1) := (others => '0');

  signal use_snapshot : std_logic_vector(0 to G_PIPELINES-1) := (others => '0');

  --* 3d std_logic vectors
  type std_logic_vector_array_bus is array(integer range <>) of std_logic_vector_array;

  signal wm_all_bconv_done_array : std_logic_vector(0 to g_PIPELINES-1) := (others => 'L');

  signal wm_hdr_wr_strb_array    : std_logic_vector(0 to g_PIPELINES-1) := (others => 'L');
  signal wm_hdr_full_array      : std_logic_vector(0 to g_PIPELINES-1) := (others => 'L');
  signal wm_hdr_nempty_array      : std_logic_vector(0 to g_PIPELINES-1) := (others => 'L');
  signal wm_hdr_data_ready_array : std_logic_vector(0 to g_PIPELINES-1) := (others => 'L');
  signal wm_hdr_payload_array    : std_logic_vector_array(0 to g_PIPELINES-1)(i_sys_hdr'length
                                                                              + daq_info_rt'w
                                                                              + g_NBITS_SNAPSHOT
                                                                              + i_usr_hdr_payload'length
                                                                              + 8-1 downto 0) := (others => (others => '0'));

  signal wm_stream_hdr_full_array_bus      : std_logic_vector_array(0 to g_PIPELINES-1)(g_STREAM_WIDTH_ARRAY'range)             := (others => (others => 'L'));
  signal wm_stream_hdr_wr_strb_array_bus    : std_logic_vector_array(0 to g_PIPELINES-1)(g_STREAM_WIDTH_ARRAY'range)             := (others => (others => 'L'));
  signal wm_stream_hdr_nempty_array_bus     : std_logic_vector_array(0 to g_PIPELINES-1)(g_STREAM_WIDTH_ARRAY'range)             := (others => (others => 'L'));
  signal wm_stream_hdr_data_ready_array_bus : std_logic_vector_array(0 to g_PIPELINES-1)(g_STREAM_WIDTH_ARRAY'range)             := (others => (others => 'L'));
  signal wm_stream_hdr_payload_emurray_bus  : std_logic_vector_array(0 to g_PIPELINES-1)(sum(STREAM_HDR_WIDTH_ARRAY)-1 downto 0) := (others => (others => '0'));

  signal wm_stream_body_full_array_bus      : std_logic_vector_array(0 to g_PIPELINES-1)(g_STREAM_WIDTH_ARRAY'range)     := (others => (others => 'L'));
  signal wm_stream_body_wr_strb_array_bus    : std_logic_vector_array(0 to g_PIPELINES-1)(g_STREAM_WIDTH_ARRAY'range)     := (others => (others => 'L'));
  signal wm_stream_body_nempty_array_bus     : std_logic_vector_array(0 to g_PIPELINES-1)(g_STREAM_WIDTH_ARRAY'range)     := (others => (others => 'L'));
  signal wm_stream_body_data_ready_array_bus : std_logic_vector_array(0 to g_PIPELINES-1)(g_STREAM_WIDTH_ARRAY'range)     := (others => (others => 'L'));
  signal wm_stream_body_valid_array_bus      : std_logic_vector_array(0 to g_PIPELINES-1)(g_STREAM_WIDTH_ARRAY'range)     := (others => (others => '0'));
  signal wm_stream_body_payload_emurray_bus  : std_logic_vector_array(0 to g_PIPELINES-1)(i_stream_payload_emurray'range) := (others => (others => '0'));

  signal nm_hdr_full_array_bus      : std_logic_vector_array(i_f2e_pfull_array'range)(0 to g_PIPELINES-1) := (others => (others => 'L'));
  signal nm_hdr_wr_strb_array_bus    : std_logic_vector_array(i_f2e_pfull_array'range)(0 to g_PIPELINES-1) := (others => (others => 'L'));
  signal nm_hdr_nempty_array_bus     : std_logic_vector_array(i_f2e_pfull_array'range)(0 to g_PIPELINES-1) := (others => (others => 'L'));
  signal nm_hdr_data_ready_array_bus : std_logic_vector_array(i_f2e_pfull_array'range)(0 to g_PIPELINES-1) := (others => (others => 'L'));
  signal nm_hdr_payload_array_bus    : std_logic_vector_array_bus(i_f2e_pfull_array'range)(0 to g_PIPELINES-1)(i_sys_hdr'length
                                                                                                             + daq_info_rt'w
                                                                                                             + i_usr_hdr_payload'length-1 downto 0) := (others => (others => (others => '0')));

  signal wm_active_array     : std_logic_vector(0 to g_PIPELINES-1) := (others => '0');

  -- data bus from bus converter ===============================================

  --* Calculates the number of ones in a bit-vector. This function is used in
  --* the busy flag processing
  function count_ones(s : std_logic_vector) return natural is
    constant L : integer := s'length/2;
  begin
    if (s'length = 1) then
      if ( s = "1") then
        return 1 ;
      else
        return 0 ;
      end if ;
    else
      if s'ascending = true then
        return count_ones(s(s'low to s'low+L-1)) + count_ones(s(s'low+L to s'high));
      else
        return count_ones(s(s'high downto s'low+L))+ count_ones(s(s'low+L-1 downto s'low));
      end if;
    end if ;
  end function ;

  signal pbldr_active_array : std_logic_vector(i_f2e_pfull_array'range);

  signal wm_wr_ptr : natural := 0;

  signal data_ready_array_bus : std_logic_vector_array(i_f2e_pfull_array'range)(0 to STREAMS-1);

  signal nempty_vector_array : std_logic_vector_array(0 to g_PIPELINES-1)(0 to 2*STREAMS);


  signal      nm_hdr_full_array : std_logic_vector      (i_f2e_pfull_array'range);
  signal     nm_hdr_nempty_array : std_logic_vector      (i_f2e_pfull_array'range);
  signal nm_hdr_data_ready_array : std_logic_vector      (i_f2e_pfull_array'range);
  signal    nm_hdr_wr_strb_array : std_logic_vector      (i_f2e_pfull_array'range);
  signal    nm_hdr_payload_array : std_logic_vector_array(i_f2e_pfull_array'range)(i_sys_hdr'length
                                                                                   + daq_info_rt'w
                                                                                   + G_NBITS_SNAPSHOT
                                                                                   + i_usr_hdr_payload'length
                                                                                   + 8-1 downto 0);

  signal       nm_stream_hdr_full_array_bus : std_logic_vector_array(i_f2e_pfull_array'range)(0 to STREAMS-1);
  signal     nm_stream_hdr_nempty_array_bus : std_logic_vector_array(i_f2e_pfull_array'range)(0 to STREAMS-1);
  signal nm_stream_hdr_data_ready_array_bus : std_logic_vector_array(i_f2e_pfull_array'range)(0 to STREAMS-1);
  signal    nm_stream_hdr_wr_strb_array_bus : std_logic_vector_array(i_f2e_pfull_array'range)(0 to STREAMS-1);
  signal  nm_stream_hdr_payload_emurray_bus : std_logic_vector_array(i_f2e_pfull_array'range)(sum(STREAM_HDR_WIDTH_ARRAY)-1 downto 0);

  signal       nm_stream_body_full_array_bus : std_logic_vector_array(i_f2e_pfull_array'range)(0 to STREAMS-1) := (others => (others => 'L'));
  signal     nm_stream_body_nempty_array_bus : std_logic_vector_array(i_f2e_pfull_array'range)(0 to STREAMS-1);
  signal nm_stream_body_data_ready_array_bus : std_logic_vector_array(i_f2e_pfull_array'range)(0 to STREAMS-1);
  signal    nm_stream_body_wr_strb_array_bus : std_logic_vector_array(i_f2e_pfull_array'range)(0 to STREAMS-1);
  signal  nm_stream_body_payload_emurray_bus : std_logic_vector_array(i_f2e_pfull_array'range)(i_stream_payload_emurray'range);

  signal all_data_ready_array : std_logic_vector(i_f2e_pfull_array'range);
  signal all_nodes_empty_array : std_logic_vector(i_f2e_pfull_array'range);

  signal all_bconv_done_array  : std_logic_vector(i_f2e_pfull_array'range);
  signal all_bconv_ready_array : std_logic_vector(i_f2e_pfull_array'range);

  signal wme_available, wme_available_d1, wme_available_d2 : std_logic;

begin

  --* Module to calculate write and read pointers for the window matching
  --* engines
  u_ptr_handler : entity daq_core.daq_ptr_handler
    port map (i_sys_clk_fast => i_sys_clk_fast ,
              i_sys_rst      => i_sys_rst      ,
              ------------------------------------------------------------------------
              i_busy_threshold        => i_win_busy_threshold,
              o_busy                  => o_busy,
              i_all_data_ready_array  => all_data_ready_array,
              i_all_bconv_ready_array => all_bconv_ready_array,
              i_all_bconv_done_array  => all_bconv_done_array,
              -- Request strobe, read and write pointers =============================
              i_req_strb                => i_req_strb              ,
              i_wm_active_array         => wm_active_array       ,
              i_pbldr_active_array      => pbldr_active_array    ,
              o_wm_rd_wme_ptr_vector    => wm_rd_wme_ptr_vector  ,
              o_wm_rd_pbldr_ptr_vector  => wm_rd_pbldr_ptr_vector,
              o_wm_req_strb_array       => wm_req_strb_array     ,
              o_wm_wr_ptr               => wm_wr_ptr             ,
              -- Monitoring ==========================================================
              o_use_snapshot => use_snapshot,
              o_wme_available => o_wme_available);

  
  GEN_WM_ENGINES: for jj in 0 to g_PIPELINES-1 generate
    signal stream_hdr_wr_strb_array   : std_logic_vector(STREAM_HDR_WIDTH_ARRAY'range);
    signal stream_hdr_payload_emurray : std_logic_vector(sum(STREAM_HDR_WIDTH_ARRAY)-1 downto 0);

    signal stream_body_wr_strb_array   : std_logic_vector(STREAM_HDR_WIDTH_ARRAY'range);
    signal stream_body_valid_array     : std_logic_vector(STREAM_HDR_WIDTH_ARRAY'range);
    signal stream_body_payload_emurray : std_logic_vector(i_stream_payload_emurray'range);

  begin



    u_wm : entity daq_core.daq_win_matching
      generic map (g_STREAM_WIDTH_ARRAY                => g_STREAM_WIDTH_ARRAY               ,
                   g_FIFO_DEPTH_ARRAY                  => g_FIFO_DEPTH_ARRAY                 ,
                   g_FIFO_MEMORY_TYPE_ARRAY            => g_FIFO_MEMORY_TYPE_ARRAY           ,
                   g_NBITS_STREAM_HDR_FIFO_DEPTH_ARRAY => g_NBITS_STREAM_HDR_FIFO_DEPTH_ARRAY,
                   g_NBITS_STREAM_HDR_WIDTH_ARRAY      => g_NBITS_STREAM_HDR_WIDTH_ARRAY     ,
                   g_NBITS_STREAM_HDR_COUNTER_ARRAY    => g_NBITS_STREAM_HDR_COUNTER_ARRAY   ,
                   g_NBITS_STREAM_HDR_USER_ARRAY       => g_NBITS_STREAM_HDR_USER_ARRAY      ,
                   g_NBITS_SNAPSHOT                    => g_NBITS_SNAPSHOT                   ,
                   g_FORMAT_VERSION                    => g_FORMAT_VERSION                   ,
                   g_GHDL                              => g_GHDL                             ,
                   g_ID                                => jj                                 )
      --------------------------------------------------------------------------
      port map (i_sys_clk_fast => i_sys_clk_fast,
                i_sys_rst      => i_sys_rst,
                i_sys_bx       => i_sys_bx,
                ----------------------------------------------------------------
                i_bcid_cnt     => i_bcid_cnt,
                ----------------------------------------------------------------
                i_use_snapshot => use_snapshot,
                ----------------------------------------------------------------
                i_req_strb        => wm_req_strb_array(jj),
                i_win_opening_cnt => i_win_opening_cnt,
                i_win_request_cnt => i_win_request_cnt,
                i_win_closing_cnt => i_win_closing_cnt,
                ----------------------------------------------------------------
                i_sys_hdr            => i_sys_hdr,
                i_event_id           => i_event_id,
                i_curr_bcid          => i_curr_bcid,
                i_win_opening_offset => i_win_opening_offset,
                i_win_request_offset => i_win_request_offset,
                i_win_closing_offset => i_win_closing_offset,
                i_win_window_timeout => i_win_window_timeout,
                i_win_busy_threshold => i_win_busy_threshold,
                ----------------------------------------------------------------
                i_usr_hdr_payload    => i_usr_hdr_payload,
                ----------------------------------------------------------------
                i_stream_user_header_emurray => i_stream_user_header_emurray,
                i_stream_ctrl_bcid_array     => i_stream_ctrl_bcid_array,
                i_stream_data_bcid_array     => i_stream_data_bcid_array,
                i_stream_valid_array         => i_stream_valid_array,
                i_stream_enable_array        => i_stream_enable_array,
                i_stream_payload_emurray     => i_stream_payload_emurray,
                ----------------------------------------------------------------
                o_hdr_nempty     => wm_hdr_nempty_array(jj),
                o_hdr_wr_strb    => wm_hdr_wr_strb_array(jj),
                o_hdr_data_ready => wm_hdr_data_ready_array(jj),
                o_hdr_payload    => wm_hdr_payload_array(jj),
                i_hdr_full       => wm_hdr_full_array(jj),
                ----------------------------------------------------------------
                o_stream_hdr_nempty_array     =>     wm_stream_hdr_nempty_array_bus(jj),
                o_stream_hdr_wr_strb_array    =>    wm_stream_hdr_wr_strb_array_bus(jj),
                o_stream_hdr_data_ready_array => wm_stream_hdr_data_ready_array_bus(jj),
                o_stream_hdr_payload_emurray  =>  wm_stream_hdr_payload_emurray_bus(jj),
                i_stream_hdr_full_array       =>       wm_stream_hdr_full_array_bus(jj),
                ----------------------------------------------------------------
                o_stream_body_nempty_array     =>     wm_stream_body_nempty_array_bus(jj),
                o_stream_body_wr_strb_array    =>    wm_stream_body_wr_strb_array_bus(jj),
                o_stream_body_data_ready_array => wm_stream_body_data_ready_array_bus(jj),
                o_stream_body_payload_emurray  =>  wm_stream_body_payload_emurray_bus(jj),
                i_stream_body_full_array       =>       wm_stream_body_full_array_bus(jj),
                ----------------------------------------------------------------
                o_data_ready   => wm_data_ready_array(jj),
                o_nodes_empty  => wm_nodes_empty_array(jj),
                o_nempty_array => nempty_vector_array(jj),
                ------------------------------------------------------------------------
                o_wm_active => wm_active_array(jj));

  end generate GEN_WM_ENGINES;

  -- packet builder ------------------------------------------------------------

  G0 : for ii in i_f2e_pfull_array'range generate
    signal pbldr_nempty_array  : std_logic_vector(0 to 2*STREAMS);
    signal pbldr_data_ready_array  : std_logic_vector(0 to 2*STREAMS);
    signal pbldr_wr_strb_array : std_logic_vector(0 to 2*STREAMS);
    signal pbldr_payload_array : std_logic_vector_array(0 to 2*STREAMS)(OUTPUT_WIDTH-1 downto 0);

    signal pbldr_pfull_array  : std_logic_vector(0 to 2*STREAMS);
    signal pbldr_pfull_array_d1   : std_logic_vector(0 to 2*STREAMS);

    signal pbldr_ce_array     : std_logic_vector(0 to 2*STREAMS);
    signal pbldr_ce_array_d1  : std_logic_vector(0 to 2*STREAMS);

    signal pbldr_nempty_array_d1  : std_logic_vector(0 to 2*STREAMS);

  begin

    process (i_sys_clk_fast)
    begin
      if rising_edge(i_sys_clk_fast) then
        all_data_ready_array(ii)  <= wm_data_ready_array(wm_rd_wme_ptr_vector(ii));
        all_nodes_empty_array(ii) <= wm_nodes_empty_array(wm_rd_wme_ptr_vector(ii));
      end if;
    end process;



    --* Module containing bus converters
    u_node_mux : entity daq_core.daq_node_mux
      generic map (g_PIPELINES                         => g_PIPELINES                        ,
                   g_STREAM_WIDTH_ARRAY                => g_STREAM_WIDTH_ARRAY               ,
                   g_NBITS_STREAM_HDR_WIDTH_ARRAY      => g_NBITS_STREAM_HDR_WIDTH_ARRAY     ,
                   g_NBITS_STREAM_HDR_FIFO_DEPTH_ARRAY => g_NBITS_STREAM_HDR_FIFO_DEPTH_ARRAY,
                   g_NBITS_STREAM_HDR_USER_ARRAY       => g_NBITS_STREAM_HDR_USER_ARRAY      ,
                   g_NBITS_STREAM_HDR_COUNTER_ARRAY    => g_NBITS_STREAM_HDR_COUNTER_ARRAY   ,
                   g_FIFO_DEPTH_ARRAY                  => g_FIFO_DEPTH_ARRAY,
                   g_FIFO_TYPE                         => g_BUS_CONVERTER_FIFO_TYPE,
                   g_GHDL                              => g_GHDL)
      port map (i_sys_clk_fast => i_sys_clk_fast,
                i_sys_rst      => i_sys_rst     ,
                -- control and monitoring ==================================================
                i_all_data_ready  => all_data_ready_array(ii),
                i_all_nodes_empty => all_nodes_empty_array(ii),
                o_all_bconv_ready => all_bconv_ready_array(ii),
                o_all_bconv_done  => all_bconv_done_array(ii),
                -- node ====================================================================
                i_hdr_payload    => nm_hdr_payload_array(ii),
                i_hdr_wr_strb    => nm_hdr_wr_strb_array(ii),
                i_hdr_nempty     =>  nm_hdr_nempty_array(ii),
                i_hdr_data_ready => nm_hdr_data_ready_array(ii),
                o_hdr_full       =>   nm_hdr_full_array(ii),
                ----------------------------------------------------------------
                i_stream_hdr_wr_strb_array    => nm_stream_hdr_wr_strb_array_bus(ii),
                i_stream_hdr_nempty_array     => nm_stream_hdr_nempty_array_bus(ii),
                i_stream_hdr_data_ready_array => nm_stream_hdr_data_ready_array_bus(ii),
                o_stream_hdr_full_array       => nm_stream_hdr_full_array_bus(ii),
                i_stream_hdr_payload_emurray  => nm_stream_hdr_payload_emurray_bus(ii),
                ----------------------------------------------------------------
                i_stream_body_wr_strb_array    => nm_stream_body_wr_strb_array_bus(ii),
                i_stream_body_nempty_array     => nm_stream_body_nempty_array_bus(ii),
                i_stream_body_data_ready_array     => nm_stream_body_data_ready_array_bus(ii),
                o_stream_body_full_array       => nm_stream_body_full_array_bus(ii),
                i_stream_body_payload_emurray  => nm_stream_body_payload_emurray_bus(ii),
                -- packet builder ==========================================================
                i_pbldr_pfull_array      => pbldr_pfull_array,
                o_pbldr_payload_array    =>  pbldr_payload_array,
                o_pbldr_wr_strb_array    =>  pbldr_wr_strb_array,
                o_pbldr_nempty_array     =>   pbldr_nempty_array,
                i_pbldr_ce_array         =>    pbldr_ce_array);

    process (i_sys_clk_fast)
    begin
      if rising_edge(i_sys_clk_fast) then
        pbldr_pfull_array_d1 <= pbldr_pfull_array;
        pbldr_ce_array_d1    <=    pbldr_ce_array;
      end if;
    end process;


    --* packet builders
    u_pbldr : entity daq_core.daq_packet_builder
      generic map (g_STREAM_HDR_WIDTH_ARRAY => STREAM_HDR_WIDTH_ARRAY     ,
                   g_STREAM_WIDTH_ARRAY     => g_STREAM_WIDTH_ARRAY       ,
                   g_FIFO_DEPTH             => g_PACKET_BUILDER_FIFO_DEPTH,
                   g_FIFO_TYPE              => g_PACKET_BUILDER_FIFO_TYPE,
                   g_GHDL                   => g_GHDL)
      port map (i_sys_clk_fast => i_sys_clk_fast,
                i_sys_rst      => i_sys_rst,
                -- control and monitoring ========================================
                o_active => pbldr_active_array(ii),
                -- nodes =========================================================
                o_ce_array         => pbldr_ce_array, -- nm_hdr_ce_array_bus(ii),
                o_pfull_array      => pbldr_pfull_array,
                i_nempty_array     => pbldr_nempty_array, -- nempty_vector_array(wm_rd_wme_ptr_vector(ii)),
                i_wr_strb_array    => pbldr_wr_strb_array,
                i_payload_array    => pbldr_payload_array,
                -- ===============================================================
                i_all_bconv_done   => all_bconv_done_array(ii),
                i_all_bconv_ready  => all_bconv_ready_array(ii),
                -- f2e ===========================================================
                i_f2e_pfull        => i_f2e_pfull_array(ii),
                o_f2e_wr_strb      => o_f2e_wr_strb_array(ii),
                o_f2e_payload      => o_f2e_payload_array(ii),
                o_f2e_ctrl         => o_f2e_ctrl_array(ii));

  end generate G0;

  --* there is one chip enable signal per fifo-like interface, which means
  --* that each head node has a chip enable signal and each data node have two
  --* chip enable signals (stream header and stream data). Chip enable has to
  --* behave as a one-hot vector, since only one fifo-like interface can be
  --* active at a given moment. "wm_rd_pbldr_ptr_vector" chooses which pointer
  --* should be used for the "wm_rd_wme_ptr_vector" routing.
  G3 : for ii in 0 to G_PIPELINES-1 generate
    PROC_DIRECT_ROUTING : process (i_sys_clk_fast)
    begin
      if rising_edge(i_sys_clk_fast) then

        wm_hdr_full_array(ii)             <=             nm_hdr_full_array(wm_rd_pbldr_ptr_vector(ii)) when ii = wm_rd_wme_ptr_vector(wm_rd_pbldr_ptr_vector(ii)) else '1';
        wm_stream_hdr_full_array_bus(ii)  <=  nm_stream_hdr_full_array_bus(wm_rd_pbldr_ptr_vector(ii)) when ii = wm_rd_wme_ptr_vector(wm_rd_pbldr_ptr_vector(ii)) else (others => '1');
        wm_stream_body_full_array_bus(ii) <= nm_stream_body_full_array_bus(wm_rd_pbldr_ptr_vector(ii)) when ii = wm_rd_wme_ptr_vector(wm_rd_pbldr_ptr_vector(ii)) else (others => '1');
      end if;
    end process PROC_DIRECT_ROUTING;
  end generate G3;



  --* choosing pipeline to which signals are connected from one of the output
  --* links. All stream nodes need to be connected to the same pipeline.
  G6 : for ii in i_f2e_pfull_array'range generate

    PROC_ROUTE_HEADER : process (i_sys_clk_fast)
    begin
      if rising_edge(i_sys_clk_fast) then
        nm_hdr_nempty_array(ii) <=         wm_hdr_nempty_array(wm_rd_wme_ptr_vector(ii));
        nm_hdr_data_ready_array(ii) <= wm_hdr_data_ready_array(wm_rd_wme_ptr_vector(ii));
        nm_hdr_wr_strb_array(ii) <=       wm_hdr_wr_strb_array(wm_rd_wme_ptr_vector(ii));
        nm_hdr_payload_array(ii) <=       wm_hdr_payload_array(wm_rd_wme_ptr_vector(ii));

        nm_stream_hdr_payload_emurray_bus(ii)  <=  wm_stream_hdr_payload_emurray_bus(wm_rd_wme_ptr_vector(ii));
        nm_stream_body_payload_emurray_bus(ii) <= wm_stream_body_payload_emurray_bus(wm_rd_wme_ptr_vector(ii));
      end if;
    end process PROC_ROUTE_HEADER;

    G4 : for jj in 0 to STREAMS-1 generate
      PROC_ROUTE_STREAMS : process (i_sys_clk_fast)
      begin
        if rising_edge(i_sys_clk_fast) then
          nm_stream_hdr_nempty_array_bus(ii)(jj)     <=     wm_stream_hdr_nempty_array_bus(wm_rd_wme_ptr_vector(ii))(jj);
          nm_stream_hdr_data_ready_array_bus(ii)(jj) <= wm_stream_hdr_data_ready_array_bus(wm_rd_wme_ptr_vector(ii))(jj);
          nm_stream_hdr_wr_strb_array_bus(ii)(jj)    <=    wm_stream_hdr_wr_strb_array_bus(wm_rd_wme_ptr_vector(ii))(jj);

          nm_stream_body_nempty_array_bus(ii)(jj)     <=     wm_stream_body_nempty_array_bus(wm_rd_wme_ptr_vector(ii))(jj);
          nm_stream_body_data_ready_array_bus(ii)(jj) <= wm_stream_body_data_ready_array_bus(wm_rd_wme_ptr_vector(ii))(jj);
          nm_stream_body_wr_strb_array_bus(ii)(jj)    <=    wm_stream_body_wr_strb_array_bus(wm_rd_wme_ptr_vector(ii))(jj);
        end if;
      end process PROC_ROUTE_STREAMS;
    end generate G4;

  end generate G6;



end architecture V2;
