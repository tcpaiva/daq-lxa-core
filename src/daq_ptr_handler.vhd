library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library daq_core;
use daq_core.daq_v3.all;
use daq_core.daq_defs.all;

--* @author Thiago Costa de Paiva
--* @id $git$
--* @version $git$
--*
--* @brief Controls the pointers to the read and write window matching engines

entity daq_ptr_handler is
  port (
    --* fast clock
    i_sys_clk_fast : in std_logic;
    --* system reset
    i_sys_rst      : in std_logic;
    -----------------------------------------------------------------------
    i_busy_threshold        : in  unsigned;
    o_busy                  : out std_logic;
    i_all_data_ready_array  : in  std_logic_vector;
    i_all_bconv_ready_array : in std_logic_vector;
    i_all_bconv_done_array : in std_logic_vector;
    -----------------------------------------------------------------------
    --* request strobe
    i_req_strb               : in  std_logic;
    --* array of active indicators for packet builders
    i_pbldr_active_array     : in  std_logic_vector;
    --* array of active indicators for window matching engines
    i_wm_active_array        : in  std_logic_vector;
    --* array of read pointers for each packet builder
    o_wm_rd_wme_ptr_vector   : out integer_vector;
    --* reverse read pointers for each window matching engines
    o_wm_rd_pbldr_ptr_vector : out integer_vector;
    --* multiplexed request strobe towards the window matching engines
    o_wm_req_strb_array      : out std_logic_vector;
    --* write window matching engine selection
    o_wm_wr_ptr              : out natural;
    -- Monitoring ==========================================================
    --* usage status of the window matching engines
    o_use_snapshot : out std_logic_vector;
    o_wme_available : out std_logic
  );
end entity daq_ptr_handler;

architecture V2 of daq_ptr_handler is

  constant OUTPUT_LINKS : integer := i_pbldr_active_array'length;
  constant PIPELINES    : integer := i_wm_active_array'length;

  --* Update snapshot with the current used window matching engine. Keeps
  --* snapshot bits for other engines as they are.
  function get_snapshot (idx: integer; curr: std_logic_vector) return std_logic_vector is
    variable y : std_logic_vector(curr'range) := (others => '0');
  begin
    for j in curr'range loop
      if j = idx then
        y(j) := '1';
      else
        y(j) := curr(j);
      end if;
    end loop;
    return y;
  end function get_snapshot;

  -- pointer to the engine that will be written in the next request
  signal wm_wr_ptr   : natural := 0;

  -- delayed pointer to the engine that will be written in the next request.
  -- Used to calculate current snapshot
  signal wm_wr_ptr_d : natural := 0;

  --* Pointer to the next engine to be used by the write pointer in
  --* initialization. This is needed because next engine should be the same as
  --* the one being used in case of just one engine.
  function init_wm_wr_nxt return natural is
  begin
    if PIPELINES > 1 then
      return 1;
    else
      return 0;
    end if;
  end function init_wm_wr_nxt;
  signal wm_wr_nxt   : natural := init_wm_wr_nxt;

  --* Output packet builder is pointed out towards the engines it has to read.
  --* This function provides the reverse pointer so that the engines is linked
  --* to the output packet builder it is using to send data out. This is needed
  --* for the several multiplexing layers.
  impure function get_reverse_ptr (wm_rd_wme_ptr_vector : integer_vector;
                                   x                    : integer) return integer is
    variable y : integer := 0;
  begin
    for ii in i_pbldr_active_array'range loop
      if wm_rd_wme_ptr_vector(ii) = x then
        y := ii;
      end if;
    end loop;
    return y;
  end function get_reverse_ptr;

  type state_t is (WAIT_DATA_READY, WAIT_BCONV_READY, WAIT_BCONV_DONE);
  type state_at is array(i_wm_active_array'range) of state_t;
  signal state : state_at := (others => WAIT_DATA_READY);

  signal wm_rd_wme_ptr_vector   : integer_vector(i_pbldr_active_array'range) := (others => 0);
  signal wm_rd_pbldr_ptr_vector : integer_vector(i_wm_active_array'range) := (others => 0);

  signal th_b : signed(12 downto 0);

  type availability_t is (AVAILABLE, NOT_AVAILABLE);
  signal availability : availability_t := AVAILABLE;

  signal busy_vector : std_logic_vector(i_pbldr_active_array'range) := (others => '0');

  signal wm_wr_trn    : std_logic := '0';
  signal wm_wr_trn_d  : std_logic := '0';
  signal wm_rd_trn_v  : std_logic_vector(i_pbldr_active_array'range) := (others => '0');
  signal wm_rd_trn_vd : std_logic_vector(i_pbldr_active_array'range) := (others => '0');

  type bolean_vector is array(natural range <>) of boolean;
  signal wr_eq_rd_v : boolean_vector(i_pbldr_active_array'range);
  signal wr_gt_rd_v : boolean_vector(i_pbldr_active_array'range);
  signal wr_lt_rd_v : boolean_vector(i_pbldr_active_array'range);

  signal rd_minus_wr_v : integer_vector(i_pbldr_active_array'range);
  signal wr_minus_rd_v : integer_vector(i_pbldr_active_array'range);
  
begin

  o_wm_wr_ptr <= wm_wr_ptr;
  o_wm_rd_wme_ptr_vector <= wm_rd_wme_ptr_vector;
  o_wm_rd_pbldr_ptr_vector <= wm_rd_pbldr_ptr_vector;

  --* snapshot of pipelines usage is calculated before the request arrives so it
  --* can be sampled with the correct value by the corresponding pipeline.
  o_use_snapshot <= get_snapshot(wm_wr_ptr_d, i_wm_active_array);


  process (i_sys_clk_fast)
  begin
    if rising_edge(i_sys_clk_fast) then
      if i_sys_rst = '1' then
        availability <= AVAILABLE;
      else

        case availability is
          when AVAILABLE =>
            o_wme_available <= '1';
            if i_wm_active_array(wm_wr_ptr) = '0' and i_req_strb = '1' then
              if i_wm_active_array(wm_wr_nxt) = '1' then
                -- check if next engine is available, if not clear available flag;
                o_wme_available <= '0';
                availability <= NOT_AVAILABLE;
              end if;
            end if;
          when NOT_AVAILABLE =>
            if i_wm_active_array(wm_wr_ptr) = '0' then
              o_wme_available <= '1';
              availability <= AVAILABLE;
            end if;
        end case;

      end if; -- rst
    end if; -- clk
  end process;



  -- threshold under the point of view of free engines instead of occupied ones
  th_b <= to_signed(PIPELINES - to_integer(i_busy_threshold), 13);
  
  -- busy flag should activate when any of the internal busy signals are on
  o_busy <= '1' when or(busy_vector) else '0';
  
  GEN_BUSY : for ii in i_pbldr_active_array'range generate
  begin

    PROC_BUSY : process (i_sys_clk_fast)
    begin
      if rising_edge(i_sys_clk_fast) then

        if i_sys_rst = '1' then
          busy_vector(ii) <= '0';
        else

          wr_eq_rd_v(ii) <= wm_wr_ptr = wm_rd_wme_ptr_vector(ii);
          wr_gt_rd_v(ii) <= wm_wr_ptr > wm_rd_wme_ptr_vector(ii);
          wr_lt_rd_v(ii) <= wm_wr_ptr < wm_rd_wme_ptr_vector(ii);

          rd_minus_wr_v(ii) <= wm_rd_wme_ptr_vector(ii) - wm_wr_ptr;
          wr_minus_rd_v(ii) <= wm_wr_ptr - wm_rd_wme_ptr_vector(ii);

          wm_wr_trn_d  <= wm_wr_trn;
          wm_rd_trn_vd(ii) <= wm_rd_trn_v(ii);

          if busy_vector(ii) = '1' and i_busy_threshold /= 0 then

            --* if busy is activated, deactivate it only when engines are not used.
            if wr_eq_rd_v(ii) and wm_rd_trn_vd(ii) = wm_wr_trn_d then
              busy_vector(ii) <= '0';
            end if;

          else

            if i_busy_threshold = 0 then
              busy_vector(ii) <= '1';
            elsif i_busy_threshold > PIPELINES then
              busy_vector(ii) <= '0';
            else
            
              --* if rd_ptr > wr_ptr : use minimum required number of free engines
              if wr_lt_rd_v(ii) and rd_minus_wr_v(ii) <= th_b then
                busy_vector(ii) <= '1';
              end if;

              --* if wr_ptr > rd_ptr : use maximum required number of occupied engines
              if wr_gt_rd_v(ii) and wr_minus_rd_v(ii) >= i_busy_threshold then
                busy_vector(ii) <= '1';
              end if;

              --* if wr_ptr = rd_ptr : busy is active if pointers are not in the
              -- same counting cycle (overflow)
              if wr_eq_rd_v(ii) and wm_rd_trn_vd(ii) /= wm_wr_trn_d then
                busy_vector(ii) <= '1';
              end if;
              
            end if;

          end if;

        end if; -- rst

      end if; -- clk
    end process PROC_BUSY;
  end generate GEN_BUSY;


  --* Multiplex request signal towards the next window matching engine.
  --* Check if write pointer points to the window matching engine and if the
  --* that it is free to use. In that case, request is forwarded.
  GEN_REQ_MUX: for jj in i_wm_active_array'range generate
  begin
    FWD_REQUEST : process (i_sys_clk_fast)
    begin
      if rising_edge(i_sys_clk_fast) then
        if i_sys_rst = '1' then
          o_wm_req_strb_array(jj) <= '0';
        else
          if wm_wr_ptr = jj and i_wm_active_array(jj) = '0' then
            o_wm_req_strb_array(jj) <= i_req_strb;
          else
            o_wm_req_strb_array(jj) <= '0';
          end if;
        end if;
      end if;
    end process FWD_REQUEST;
  end generate GEN_REQ_MUX;


  --* Process to calculate the pointer to which next matching window should be.
  --* It only updates the pointer to the next matching window when the request
  --* strobe is received.
  NXT_WME_WR_PTR_CALC : process (i_sys_clk_fast)
  begin

    if rising_edge(i_sys_clk_fast) then

      wm_wr_ptr_d <= wm_wr_ptr;

      if i_sys_rst = '1' then

        wm_wr_ptr <= 0;
        wm_wr_nxt <= init_wm_wr_nxt;

      else

        -- when lxa request is received, update destination pointer
        if i_req_strb = '1' and i_wm_active_array(wm_wr_ptr) = '0' then

          if wm_wr_nxt = 0 then
            wm_wr_trn <= not wm_wr_trn;
          end if;

          wm_wr_ptr <= wm_wr_nxt;

          -- wm_next should always be the next pipeline
          if wm_wr_nxt < PIPELINES-1 then
            wm_wr_nxt <= wm_wr_nxt + 1;
          else
            wm_wr_nxt <= 0;
          end if;

        end if; -- req_strb received

      end if;  -- rst

    end if; -- clk

  end process NXT_WME_WR_PTR_CALC;


  --* Processes to update the pointers related to the packet builders,
  --* indicating which window matching engine should be read by it. Pointer is
  --* only updated after window matching engine pointed out activates and then
  --* deactivates.
  G0: for ii in i_pbldr_active_array'range generate

    UPDATE_WME_RD_PTR : process (i_sys_clk_fast)
      variable idx : integer := 0;
    begin

      if rising_edge(i_sys_clk_fast) then

        if i_sys_rst = '1' then

          wm_rd_wme_ptr_vector(ii) <= ii;
          state(ii) <= WAIT_DATA_READY;

        else

          case state(ii) is

            when WAIT_DATA_READY =>
              if i_all_data_ready_array(ii) = '1' then
                state(ii) <= WAIT_BCONV_READY;
              end if;

            when WAIT_BCONV_READY =>
              if i_all_bconv_ready_array(ii) = '1' then
                state(ii) <= WAIT_BCONV_DONE;
              end if;

            when WAIT_BCONV_DONE =>
              if i_all_bconv_done_array(ii) = '1' then
                if wm_rd_wme_ptr_vector(ii) < PIPELINES - OUTPUT_LINKS then
                  wm_rd_wme_ptr_vector(ii) <= wm_rd_wme_ptr_vector(ii) + OUTPUT_LINKS;
                else
                  wm_rd_wme_ptr_vector(ii) <= wm_rd_wme_ptr_vector(ii) + OUTPUT_LINKS - PIPELINES;
                  wm_rd_trn_v(ii) <= not wm_rd_trn_v(ii);
                end if;
                state(ii) <= WAIT_DATA_READY;
              end if;

          end case;

        end if;  -- rst

      end if; -- clk

    end process UPDATE_WME_RD_PTR;

  end generate G0;

  --* Generate reverse pointer for each window matching engine towards the
  --* current linked packet builder.
  G1: for ii in i_wm_active_array'range generate
    UPDATE_REVERSE_RD_PTR : process(i_sys_clk_fast)
    begin
      if rising_edge(i_sys_clk_fast) then
        wm_rd_pbldr_ptr_vector(ii) <= get_reverse_ptr(wm_rd_wme_ptr_vector, ii);
      end if;
    end process UPDATE_REVERSE_RD_PTR;
  end generate G1;

end architecture V2;
