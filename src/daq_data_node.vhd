library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library daq_core;
use daq_core.daq_v3.all;

--* @author Thiago Costa de Paiva
--* @id $git$
--* @version $git$
--*
--* @brief Data nodes select the data and store them. Selection happen based on
--* two id counters (ctrl and data) that should be part of the matching window

entity daq_data_node is
  generic (
    --* Depth of the FIFO used by the data node
    g_FIFO_DEPTH                    : integer;
    --* Memory type of the FIFO used by the data node. Valid options are
    --* "auto", "bram", "uram" (FPGA family dependent), or "distributed".
    g_FIFO_MEMORY_TYPE              : string;
    --* Number of bits to represent the FIFO depth in the packet
    g_NBITS_STREAM_HDR_FIFO_DEPTH   : integer;
    --* Number of bits to represent the stream width in the packet
    g_NBITS_STREAM_HDR_WIDTH        : integer;
    --* Number of bits to represent the word counter in the packet
    g_NBITS_STREAM_HDR_COUNTER      : integer;
    --* Number of bits to represent the stream user header in the packet
    g_NBITS_STREAM_HDR_USER         : integer := 0;
    g_GHDL                          : boolean
  );
  port (
    --* fast clock
    i_sys_clk_fast : in std_logic;
    --* system reset
    i_sys_rst      : in std_logic;
    ------------------------------------------------------------------------
    --* flag indicating that this window matching engine is active
    i_wm_active    : in  std_logic;
    --* flag indicating that the matchin window has tied out
    i_wm_timed_out : in  std_logic;
    --* value for the opening edge of the matching window
    i_wm_opening   : in  unsigned(11 downto 0);
    --* value for the closing edge of the matching window
    i_wm_closing   : in  unsigned(11 downto 0);
    --* reserved
    o_error        : out std_logic := '0';
    --* flag indicating that all the nodes are ready to be drained
    i_read_mode    :  in std_logic := '0';
    ------------------------------------------------------------------------
    --+ stream information
    i_stream_user_header : in  std_logic_vector;
    i_stream_ctrl_bcid   : in  unsigned(11 downto 0);
    i_stream_data_bcid   : in unsigned(11 downto 0);
    i_stream_valid       : in std_logic;
    i_stream_enable      : in std_logic;
    i_stream_payload     : in std_logic_vector;
    ------------------------------------------------------------------------
    --* flag indicating that the stream header FIFO cannot store data
    i_hdr_full      :  in std_logic;
    --* write strobe aimed to the stream header bus converter FIFO 
    o_hdr_wr_strb    : out std_logic;
    --* flag indicating that header payload is ready to be retrieved
    o_hdr_data_ready : out std_logic;
    --* flag indicating that header information is stored
    o_hdr_nempty     : out std_logic;
    --* header payload aimed to the bus converter FIFO
    o_hdr_payload    : out std_logic_vector(g_NBITS_STREAM_HDR_FIFO_DEPTH
                                            + g_NBITS_STREAM_HDR_WIDTH
                                            + g_NBITS_STREAM_HDR_USER
                                            + g_NBITS_STREAM_HDR_COUNTER
                                            - 1 downto 0) := (others => '0');
    ------------------------------------------------------------------------
    --* flag indicating that the stream body FIFO cannot store data
    i_body_full      :  in std_logic;
    --* write strobe aimed to the stream body bus converter FIFO 
    o_body_wr_strb    : out std_logic;
    --* flag indicating that stream body payload is ready to be retrieved
    o_body_data_ready : out std_logic;
    --* data payload aimed to the stream body bus converter FIFO
    o_body_payload    : out std_logic_vector;
    --* flag indicating that stream data is stored
    o_body_nempty     : out std_logic
  );
end entity daq_data_node;

architecture V3 of daq_data_node is

  -- flag to indicate if words of interest
  -- registered signals to handle negative slack and to synchronize with data
  signal worth_payload    : std_logic := '0';
  signal worth_payload_d1 : std_logic := '0';
  signal worth_payload_d2 : std_logic := '0';
  signal open_window      : std_logic := '0';
  signal open_window_d1   : std_logic := '0';
  signal open_window_d2   : std_logic := '0';

  signal body_rst      : std_logic := '0';

  signal fifo_wr_strb     : std_logic := '0';
  signal fifo_wr_strb_aux : std_logic := '0';
  
  signal body_wr_strb_aux : std_logic := '0';
  
  signal fifo_wr_count : unsigned(g_NBITS_STREAM_HDR_COUNTER-1 downto 0) := (others => '0');

  signal hdr_payload : std_logic_vector(g_NBITS_STREAM_HDR_FIFO_DEPTH              
                                               + g_NBITS_STREAM_HDR_WIDTH                 
                                               + i_stream_user_header'length
                                               + g_NBITS_STREAM_HDR_COUNTER - 1 downto 0) := (others => '0');

  signal body_rd_strb     : std_logic := '0';
  signal body_rd_strb_aux : std_logic := '0';

  --* IDLE: data node is available to process stream data
  --*
  --* COLLECT: data being collected (window is open)
  --*
  --* REGISTERING: storing header information 
  --*
  --* WAITING: prevent FSM to advance while packet builder is not active
  --*
  --* SEND_HDR: write header data to the packet builder FIFO
  --*
  --* SEND_BODY: write body data to the packet builder FIFO
  --*
  --* HALT: wait window deactivation
  type state_t is (IDLE, COLLECT, REGISTERING, WAITING, SEND_HDR, SEND_BODY, HALT);
  signal state : state_t := IDLE;
  
  signal c : std_logic_vector(0 to 5);

  signal stream_valid_d1       : std_logic;
  signal stream_valid_d2       : std_logic;
  signal stream_enable_d1      : std_logic;
  signal stream_enable_d2      : std_logic;
  signal stream_payload_d1     : std_logic_vector(i_stream_payload'range);
  signal stream_payload_d2     : std_logic_vector(i_stream_payload'range);
  signal stream_user_header_d1 : std_logic_vector(i_stream_user_header'range);
  signal stream_user_header_d2 : std_logic_vector(i_stream_user_header'range);


  --* function to indicate if the bcid provided (ctrl or data) is of interest.
  --* <pre>
  --*   x < o  |  x > o  |  x > o
  --*   x < c  |  x < c  |  x > c
  --*          o         c              
  --* ---------+=========+-------------
  --* 
  --*   x < o  |  x < o  |  x > o
  --*   x < c  |  x > c  |  x > c
  --*          c         o              
  --* =========+---------+=========
  --* </pre>
  function is_bcid_of_interest(opening, closing, bcid : unsigned) return std_logic is
    variable y : std_logic;
  begin
    if opening <= closing and opening <= bcid and bcid <= closing then
      y := '1';
    elsif closing < opening and (bcid <= closing or opening <= bcid) then
      y := '1';
    else
      y := '0';
    end if;
    return y;
  end function is_bcid_of_interest;

  signal wme_active_d1 : std_logic;
  signal wme_active_d2 : std_logic;

  signal blocked : std_logic := '1';

  signal wm_opening : unsigned(i_wm_opening'range);
  signal wm_closing : unsigned(i_wm_closing'range);
  
  signal body_nempty : std_logic := '0';
  signal body_nempty_aux : std_logic := '0';

  signal fifo_full : std_logic;
  
begin

  o_error <= '0';

  worth_payload     <= is_bcid_of_interest(i_wm_opening, i_wm_closing, i_stream_data_bcid);
  open_window       <= is_bcid_of_interest(i_wm_opening, i_wm_closing, i_stream_ctrl_bcid);

  --* registering FIFO write strobe logic
  PROC_WR_STRB_LOGIC : process(i_sys_clk_fast)
  begin
    if rising_edge(i_sys_clk_fast) then
      worth_payload_d1 <= worth_payload;
      open_window_d1   <= open_window;
      
      wme_active_d1     <= i_wm_active;
      stream_payload_d1 <= i_stream_payload;
      stream_valid_d1   <= i_stream_valid;  
      stream_enable_d1      <= i_stream_enable; 
      stream_user_header_d1 <= i_stream_user_header;

      c(0) <= stream_valid_d1;
      c(1) <= worth_payload_d1;
      c(2) <= open_window_d1;
      c(3) <= i_wm_timed_out;
      c(4) <= o_body_data_ready;
      c(5) <= blocked;
      
      worth_payload_d2      <= worth_payload_d1;
      open_window_d2        <= open_window_d1;  
      stream_payload_d2     <= stream_payload_d1;
      stream_enable_d2      <= stream_enable_d1; 
      stream_user_header_d2 <= stream_user_header_d1;
      wme_active_d2         <= wme_active_d1;
      
    end if; -- clk
  end process PROC_WR_STRB_LOGIC;

  fifo_wr_strb <= '1' when c = "111000" else '0';
  
  -- data FIFO -----------------------------------------------------------------
  --* FIFO to store selected dta
  stream_nfifo_u : entity daq_core.daq_fifo
    generic map (g_FIFO_DEPTH       => g_FIFO_DEPTH,
                 g_FIFO_MEMORY_TYPE => g_FIFO_MEMORY_TYPE,
                 g_GHDL             => g_GHDL)
    ------------------------------------------------------
    port map (i_sys_clk_fast => i_sys_clk_fast,
              i_sys_rst      => i_sys_rst,
              --------------------------------------------
              i_src_payload  => stream_payload_d2,
              i_src_wr_strb  => fifo_wr_strb,
              o_src_full     => fifo_full,
              --------------------------------------------
              i_dst_rd_strb  => body_rd_strb,
              o_dst_nempty   => body_nempty,
              o_dst_wr_cnt   => fifo_wr_count,
              o_dst_payload  => o_body_payload);

  body_rd_strb   <= '1' when body_rd_strb_aux = '1' and body_nempty = '1' else '0';
  o_body_wr_strb   <= '1' when body_wr_strb_aux = '1' and body_nempty = '1' else '0';

  o_body_nempty <= '1' when fifo_wr_strb = '1' or body_nempty_aux = '1' or body_nempty = '1' else '0';
  
  process (i_sys_clk_fast)
  begin
    if rising_edge(i_sys_clk_fast) then
      if fifo_wr_strb = '1' then
        body_nempty_aux <= '1';
      elsif body_nempty = '1' then
        body_nempty_aux <= '0';
      end if;
    end if;
  end process;
    
  --* TODO: HDR AND BODY CAN BE TRANSFERRED SIMULTANEOUSLY
  

  --* FSM to coordinate the activities of the data node
  PROC_FSM : process (i_sys_clk_fast)
  begin
    if rising_edge(i_sys_clk_fast) then

      if i_sys_rst = '1' then

        state      <= IDLE;
        o_body_data_ready <= '0';

        body_rd_strb_aux <= '0';
        body_wr_strb_aux <= '0';

      else
        
        o_hdr_wr_strb <= '0';
        body_rd_strb_aux <= '0';
        body_wr_strb_aux <= '0';
                
        case state is

          when IDLE =>
            o_hdr_nempty <= '0';
            o_hdr_data_ready <= '0';
            o_body_data_ready <= '0';
            blocked <= '1';
            if wme_active_d2 = '1' then -- wait 2 cycles for the open_window update
              blocked <= '0';
              if i_wm_timed_out = '1' or open_window_d2 = '1' or stream_enable_d2 = '0' then
                state <= COLLECT;
              end if;
            end if;

          when COLLECT =>
            if i_wm_timed_out = '1' or open_window_d2 = '0' or stream_enable_d2 = '0' then
              state <= REGISTERING;
              o_body_data_ready <= '1';
            end if;

          when REGISTERING =>
            blocked <= '1';
            o_hdr_payload(hdr_payload'range) <= 
              std_logic_vector(to_unsigned(i_stream_payload'length, g_NBITS_STREAM_HDR_WIDTH))
              & std_logic_vector(fifo_wr_count)
              & std_logic_vector(to_unsigned(g_FIFO_DEPTH, g_NBITS_STREAM_HDR_FIFO_DEPTH))
              & stream_user_header_d2;            
            o_hdr_data_ready <= '1';
            o_hdr_nempty <= '1';
            state <= WAITING;
            
          when WAITING =>
            if i_read_mode = '1' then
              state <= SEND_HDR;
            end if;

          when SEND_HDR =>
            if i_hdr_full = '0' then
              o_hdr_wr_strb <= '1';
              o_hdr_nempty <= '0';
              state <= SEND_BODY;
            end if;

          when SEND_BODY =>
            if body_nempty = '1' then
              if i_body_full = '0' then
                body_wr_strb_aux <= '1';
                body_rd_strb_aux <= '1';
              end if;
            else
              state <= HALT;
            end if;
            
          when HALT =>
            if wme_active_d2 = '0' then
              o_hdr_data_ready <= '0';
              o_body_data_ready <= '0';
              state <= IDLE;
            end if;
        end case;
      end if; -- rst
    end if; -- clk
  end process PROC_FSM;


  
end V3;
