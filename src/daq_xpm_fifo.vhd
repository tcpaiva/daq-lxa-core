library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library xpm;
use xpm.vcomponents.all;

--* @author  Thiago Costa de Paiva
--* @id      $git$
--* @version $git$
--*
--* @brief Wrapper to facilitate and standardize the use of the XPM Synchronous
--* First-word-falls-through FIFO from Xilinx. XPM FIFO is used instead of the
--* related IP core. FIFO "full", "almost full", and "half full" flags are
--* exposed.

entity daq_xpm_fifo is
--------------------------------------------------------------------------------
  generic (
    --+ FIFO description
    g_FIFO_DEPTH       : integer;
    g_FIFO_MEMORY_TYPE :  string
  );
  ------------------------------------------------------------------------------
  port (
    --* FIFO operating clock. It says "fast" to reference the single clock
    --* domain used in the design.
    i_sys_clk_fast        : in         std_logic;
    --* system reset
    i_sys_rst             : in         std_logic;
    ------------------------------------------------------------------------
    --* FIFO almost full flag. It is raised when all but one slots of the FIFO
    --* are used.
    o_src_afull           : out        std_logic;
    --* FIFO half full flag. It is raised when 68.75% of g_FIFO_DEPTH is reached.
    o_src_pfull           : out        std_logic;
    --* FIFO full flag. It is raised when the FIFO is fully occupied. 
    o_src_full            : out        std_logic;
    --* Content to be stored in FIFO. Width of payload is defined by the signal
    --* connected to it using the VHDL2008 style.
    i_src_payload         : in  std_logic_vector;
    --* FIFO write strobe
    i_src_wr_strb         : in  std_logic       ;
    ------------------------------------------------------------------------
    --* FIFO read strobe. This signal should be acted after the output payload
    --* is used.
    i_dst_rd_strb         : in         std_logic;
    --* FIFO nempty flag. It is raised when there is data available in the FIFO.
    o_dst_nempty          : out        std_logic;
    --* Counter for words written. Value is zeroed when nempty flag becomes low.
    o_dst_wr_cnt          : out unsigned;
    --* Content read from FIFO. Width of payload is defined by the signal
    --* connected to it using the VHDL2008 style.
    o_dst_payload         : out std_logic_vector
  );
--------------------------------------------------------------------------------
end entity daq_xpm_fifo;

architecture V1 of daq_xpm_fifo is

  --* Number of bits for the internal FIFO write counter. The write counter
  --* itself is not used in this design since it is fully disabled by the
  --* advanced features configuration of the XPM FIFO.
  constant WRITE_DATA_COUNT_WIDTH : integer := integer(log2(real(g_FIFO_DEPTH)));

  signal empty       : std_logic;
  -- signal wr_count    : std_logic_vector(WRITE_DATA_COUNT_WIDTH-1 downto 0);

  signal src_afull : std_logic;
  signal src_pfull : std_logic;
  signal src_full : std_logic;

  signal wr_cnt : unsigned(o_dst_wr_cnt'range) := (others => '0');

  signal dst_nempty_d : std_logic;
  
  signal src_payload : std_logic_vector(i_src_payload'range);
  signal src_wr_strb : std_logic;
  
begin

  o_dst_nempty <= '1' when empty = '0' else '0';

  o_src_afull <= src_afull;
  o_src_pfull <= src_pfull;
  o_src_full  <=  src_full;

  o_dst_wr_cnt <= wr_cnt;
  

  --* Re-implementing FIFO write counter to get number of written words
  --* instantaneously. It keeps counting even when FIFO is full, this way
  --* overflow can be detected. The counter is zeroed when not empty flag
  --* becomes low, that is, when it toggles from high to low.
  PROC_WR_CNTR : process (i_sys_clk_fast)
  begin
    if rising_edge(i_sys_clk_fast) then
      dst_nempty_d <= o_dst_nempty;
      
      if i_sys_rst = '1' or (dst_nempty_d = '1' and o_dst_nempty = '0') then
        wr_cnt <= (wr_cnt'range => '0');
      else
        src_payload <= i_src_payload;
        src_wr_strb <= i_src_wr_strb;
      
        if i_src_wr_strb = '1' then
          wr_cnt <= wr_cnt + 1;
        end if;
      end if;
    end if;
  end process PROC_WR_CNTR;

  --* Xilinx XPM FIFO with almost full and half full flag enabled. All other
  --* advanced features are disabled to reduce resource usage.
  --* <pre>
  --* adv
  --* 000A = 0000 0000 0000 1010
  --*           | ||||    | ||||overflow flag
  --*           | ||||    | |||prog_full flag
  --*           | ||||    | ||wr_data_count enable
  --*           | ||||    | |almost_full flag
  --*           | ||||    |wr_ack flag
  --*           | ||||underflow flag
  --*           | |||prog_empty flag
  --*           | ||rd_data_count enable
  --*           | |almost_empty flag
  --*           |valid
  --* </pre>

  xpm_fifo_sync_inst : component xpm_fifo_sync
    generic map (
      DOUT_RESET_VALUE    => "0",                            -- String
      ECC_MODE            => "no_ecc",                       -- String
      FIFO_MEMORY_TYPE    => g_FIFO_MEMORY_TYPE,             -- String
      FIFO_READ_LATENCY   => 0,                              -- DECIMAL
      FIFO_WRITE_DEPTH    => g_FIFO_DEPTH,                   -- DECIMAL
      FULL_RESET_VALUE    => 0,                              -- DECIMAL
      READ_DATA_WIDTH     => o_dst_payload'length,           -- DECIMAL
      READ_MODE           => "fwft",                         -- String
      SIM_ASSERT_CHK      => 0,                              -- DECIMAL
      USE_ADV_FEATURES    => "000A",                         -- see above
      WAKEUP_TIME         => 0,                              -- DECIMAL
      WRITE_DATA_WIDTH    => i_src_payload'length,           -- DECIMAL
      WR_DATA_COUNT_WIDTH => WRITE_DATA_COUNT_WIDTH,         -- DECIMAL
      PROG_FULL_THRESH    => integer(0.6875*(real(g_FIFO_DEPTH))))                 -- DECIMAL
    port map (
      almost_empty  => open,
      almost_full   => src_afull,
      data_valid    => open,
      dbiterr       => open,
      dout          => o_dst_payload,
      empty         => empty,
      full          => src_full,
      overflow      => open,
      prog_empty    => open,
      prog_full     => src_pfull,
      rd_data_count => open,
      rd_rst_busy   => open,
      sbiterr       => open,
      underflow     => open,
      wr_ack        => open,
      wr_data_count => open,
      wr_rst_busy   => open,
      din           => i_src_payload,
      injectdbiterr => '0',
      injectsbiterr => '0',
      rd_en         => i_dst_rd_strb,
      rst           => i_sys_rst,
      sleep         => '0',
      wr_clk        => i_sys_clk_fast,
      wr_en         => i_src_wr_strb);

end architecture V1;
