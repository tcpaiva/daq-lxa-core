library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library daq_core;
use daq_core.daq_v3.all;

--* @author Thiago Costa de Paiva
--* @id $git$
--* @version $git$
--*
--* @brief The header node is simpler than the data nodes, since no gathering
--* stage is needed. After it is activated, the header node registers the
--* relevant information instantly and make it available to be retrieved.

entity daq_head_node is
  generic (
    --* Number of bits used in the header to represent the window matching
    --* engines status.
    g_NBITS_SNAPSHOT : natural;
    --* Version of the format used. User defined.
    g_FORMAT_VERSION : natural;
    g_ID             : integer
  );
  port (
    --* fast clock
    i_sys_clk_fast : in std_logic;
    --* system reset
    i_sys_rst    : in std_logic;
    ----------------------------------------------------------------------------
    --* status of the window matching engines
    i_use_snapshot   : in std_logic_vector;
    ----------------------------------------------------------------------------
    --* flag indicating that this window matching engine is active
    i_wm_active        : in  std_logic;
    --* flag indicating that this window matching engine timed out
    i_wm_timed_out     : in  std_logic;
    --* flag indicating that header node is ready to be retrieved
    o_data_ready       : out std_logic := '0';
    --* reserved
    o_error            : out std_logic := '0';
    --* flag indicating that this node is not empty
    o_nempty           : out std_logic := '0';
    --* read mode is activated when all nodes in a window matching engine are
    --* ready to be drained
    i_read_mode        : in  std_logic := '0';
    --* header node only gets ready after all data nodes finished, preventing
    --* the packet builder to retrieve information too early.
    i_data_nodes_ready : in  std_logic;
    ----------------------------------------------------------------------------
    --+ common information of the header
    i_bcid_cnt           : in unsigned(11 downto 0);
    i_sys_hdr            : in std_logic_vector;
    i_event_id           : in unsigned(31 downto 0);
    i_curr_bcid          : in unsigned(11 downto 0);
    i_wm_opening         : in unsigned(11 downto 0);
    i_wm_request         : in unsigned(11 downto 0); -- daq bcid
    i_wm_closing         : in unsigned(11 downto 0);
    i_wm_timeout         : in unsigned(11 downto 0);
    i_win_opening_offset : in unsigned(11 downto 0);
    i_win_request_offset : in unsigned(11 downto 0);
    i_win_closing_offset : in unsigned(11 downto 0);
    i_win_window_timeout : in unsigned(11 downto 0);
    i_win_busy_threshold : in unsigned( 7 downto 0);
    ----------------------------------------------------------------------------
    i_usr_hdr_payload : in std_logic_vector;
    ----------------------------------------------------------------------------
    --* full flag from the bus converter FIFO
    i_full   : in  std_logic;
    --* write strobe to the bus converter FIFO
    o_wr_strb : out std_logic;
    --* data payload to the bus converter FIFO
    o_payload : out std_logic_vector
  );
end entity daq_head_node;

architecture V2 of daq_head_node is

  --* IDLE: available
  --*
  --* WAITING: all data nodes must finish before header node goes ready to
  --* prevent the packet builder to start retrieving assembling the packet too
  --* early
  --* 
  --* SENDING: writing data to the bus converter FIFO
  --*
  --* HALT: waiting for window to be deactivated
  type state_t is (IDLE,
                   WAITING,
                   SENDING,
                   HALT);    
  signal state : state_t := idle;
  
  -- keep status of head node, if it has data or not.
  signal daq_info_r : daq_info_rt;
  signal daq_info_v : daq_info_vt;

  signal use_snapshot : std_logic_vector(0 to g_NBITS_SNAPSHOT-1) := (others => '0');
  
begin

  use_snapshot(i_use_snapshot'range) <= i_use_snapshot;

  
  -- there are no FIFO nor counters, therefore no errors for now.
  o_error <= '0';

  daq_info_r.bcid_cnt           <= i_bcid_cnt;
  daq_info_r.event_id           <= i_event_id;
  daq_info_r.format_version     <= to_unsigned(g_FORMAT_VERSION, daq_info_r.format_version'length);
  daq_info_r.curr_bcid          <= i_curr_bcid;
  daq_info_r.wm_opening         <= i_wm_opening;
  daq_info_r.wm_request         <= i_wm_request;
  daq_info_r.wm_closing         <= i_wm_closing;
  daq_info_r.wm_timeout         <= i_wm_timeout;
  daq_info_r.win_opening_offset <= i_win_opening_offset;
  daq_info_r.win_request_offset <= i_win_request_offset;
  daq_info_r.win_closing_offset <= i_win_closing_offset;
  daq_info_r.win_window_timeout <= i_win_window_timeout;
  daq_info_r.win_busy_threshold <= i_win_busy_threshold;

  daq_info_v <=  convert(daq_info_r, daq_info_v);


  --* Coordination of the activities in the header note
  PROC_FSM : process (i_sys_clk_fast)
    variable draining : std_logic := '0';
  begin
    if rising_edge(i_sys_clk_fast) then

      if i_sys_rst = '1' then

        state      <= idle;
        o_data_ready <= '0';
        o_payload <= (o_payload'range => '0');
        draining := '0';

      else

        o_wr_strb <= '0';
        o_payload <=  i_sys_hdr & daq_info_v & use_snapshot & i_usr_hdr_payload & std_logic_vector(to_unsigned(g_ID, 8));
              
        case state is
          when IDLE =>
            o_data_ready <= '0';
            if i_wm_active = '1' and i_data_nodes_ready = '1' and draining = '0' then
              draining := '1';
              o_data_ready <= '1';
              o_nempty <= '1';
              state <= WAITING;
             end if;
          when WAITING =>
            if i_read_mode = '1' then
              state <= SENDING;
            end if;
          when SENDING =>
            if i_full = '0' then
              o_wr_strb <= '1';
              o_nempty <= '0';
              state <= HALT;
            end if;
          when HALT =>
            if i_wm_active = '0' then
              draining := '0';
              o_data_ready <= '0';
              state <= IDLE;
            end if;
        end case;
      end if; -- rst
    end if; -- clk
    
  end process PROC_FSM;

end V2;
