library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library daq_core;
use daq_core.daq_v3.all;
use daq_core.daq_defs.all;
use daq_core.yml2hdl.all;

--* @author Thiago Costa de Paiva
--* @id $git$
--* @version $git$

--* @brief This module instantiate a bus converter for the header node and each
--* data node, handling connections between the window matching engines and the
--* packet builder.

entity daq_node_mux is
  generic (
    --* Number of parallel window matching engines
    g_PIPELINES                         : integer;
    --* Array with the stream widths
    g_STREAM_WIDTH_ARRAY                : integer_vector;
    --* array with the number of bits in the packet header representing each
    -- stream  header width
    g_NBITS_STREAM_HDR_WIDTH_ARRAY      : integer_vector;
    --* array with the number of bits in the packet header representing each
    --* stream FIFO depth
    g_NBITS_STREAM_HDR_FIFO_DEPTH_ARRAY : integer_vector;
    --* array with the number of bits in the packet header representing each
    --* stream  user header
    g_NBITS_STREAM_HDR_USER_ARRAY       : integer_vector;
    --* array with the number of bits in the packet header representing each
    --* stream counter width
    g_NBITS_STREAM_HDR_COUNTER_ARRAY    : integer_vector;
    --* Array with the bus converter FIFO depths
    g_FIFO_DEPTH_ARRAY                  : integer_vector;
    --* Type of the bus converter FIFOs: "auto", "bram", "uram", "distributed"
    g_FIFO_TYPE                         : string;
    g_GHDL                              : boolean
  );
  ------------------------------------------------------------------------------
  port (
    -- system ------------------------------------------------------------------
    --* fast clock
    i_sys_clk_fast : in std_logic;
    --* system reset
    i_sys_rst      : in std_logic;
    -- control and monitoring ==================================================
    --* data in all nodes are ready to be retrieved
    i_all_data_ready : in  std_logic;
    i_all_nodes_empty : in  std_logic;
    --* all bus converters finished the current packet
    o_all_bconv_ready : out std_logic;
    o_all_bconv_done : out std_logic;
    -- node ====================================================================
    --* flag indicating that header node bus converter cannot receive data
    o_hdr_full    : out std_logic;
    --* header node payload to the corresponding bus converter FIFO
    i_hdr_payload : in  std_logic_vector;
    --* header node write strobe to the corresponding bus converter FIFO
    i_hdr_wr_strb : in  std_logic;
    --* header node not empty flag to the corresponding bus converter
    i_hdr_nempty  : in  std_logic;
    i_hdr_data_ready  : in  std_logic;
    ----------------------------------------------------------------
    --* array with a flag from each stream header bus converter indicating that
    --* they cannot receive data
    o_stream_hdr_full_array      : out std_logic_vector;
    --* array of not empty flags from stream header nodes to the corresponding
    --* bus converters 
    i_stream_hdr_nempty_array    :  in std_logic_vector;
    i_stream_hdr_data_ready_array    :  in std_logic_vector;
    --* array of stream header write strobes to the corresponding bus converter
    --* FIFOs 
    i_stream_hdr_wr_strb_array   :  in std_logic_vector;
    --* array of stream header payloads to the corresponding bus converter
    --* FIFOs 
    i_stream_hdr_payload_emurray :  in std_logic_vector;
    ----------------------------------------------------------------
    --* array with a flag from each stream body bus converter indicating that
    --* they cannot receive data
    o_stream_body_full_array      : out std_logic_vector;
    --* array of not empty flags from the stream body nodes to the
    --* corresponding bus converters
    i_stream_body_nempty_array    :  in std_logic_vector;
    i_stream_body_data_ready_array    :  in std_logic_vector;
    --* array of stream body write strobes to the corresponding bus converter
    --* FIFOs 
    i_stream_body_wr_strb_array   :  in std_logic_vector;
    --* array of stream body payloads to the corresponding bus converter
    --* FIFOs 
    i_stream_body_payload_emurray :  in std_logic_vector;
    -- packet builder ==========================================================
    --* array with payload data from bus converters to the packet builders
    o_pbldr_payload_array : out  std_logic_vector_array;
    --* array with write strobes from bus converters to the packet builders
    o_pbldr_wr_strb_array : out  std_logic_vector;
    --* array with not empty flags from bus converters to the packet builders
    o_pbldr_nempty_array  : out  std_logic_vector;
    --* array with half full flags from the packet builders to the bus
    --* converters
    i_pbldr_pfull_array   :  in  std_logic_vector;
    --* array of enable flags from the packet builders to the bus converters
    i_pbldr_ce_array      :  in  std_logic_vector
  );
end entity daq_node_mux;

architecture V2 of daq_node_mux is

  --* shortcut to get the number of streams
  constant STREAMS : integer := g_STREAM_WIDTH_ARRAY'length;
  
  -- signal hdr_busy  : std_logic;
  signal hdr_payload : std_logic_vector(i_hdr_payload'range);
  signal hdr_wr_strb : std_logic := 'L';
  signal  hdr_nempty : std_logic := 'L';
  signal   hdr_full : std_logic := 'L';

  --* Function to calculate the number bits for the header of each stream
  function NBITS_STREAM_HDR_ARRAY return integer_vector is
    variable y : integer_vector(0 to STREAMS-1);
  begin
    for j in 0 to g_STREAM_WIDTH_ARRAY'length-1 loop
      y(j) :=  g_NBITS_STREAM_HDR_FIFO_DEPTH_ARRAY(j);
      y(j) := y(j) + g_NBITS_STREAM_HDR_WIDTH_ARRAY(j);
      y(j) := y(j) + g_NBITS_STREAM_HDR_COUNTER_ARRAY(j);
      y(j) := y(j) + g_NBITS_STREAM_HDR_USER_ARRAY(j);
    end loop;
    return y;
  end function NBITS_STREAM_HDR_ARRAY;

  signal all_bconv_done   : std_logic;
  signal all_bconv_ready  : std_logic;
  signal bconv_done_array : std_logic_vector(0 to 2*STREAMS);
  
  signal bconv_ready_array : std_logic_vector(0 to 2*STREAMS);
begin

  all_bconv_done <= '1' when bconv_done_array = (bconv_done_array'range => '1') else '0';
  o_all_bconv_done <= all_bconv_done;

  all_bconv_ready <= '1' when bconv_ready_array = (bconv_ready_array'range => '1') else '0';
  o_all_bconv_ready <= all_bconv_ready;
  
  --* Bus converter between the header node and the packet builders 
  u_bconv_hnode_fifo : entity daq_core.daq_bus_conv
    generic map (g_FIFO_DEPTH => 16, g_FIFO_TYPE => "auto", g_GHDL => g_GHDL)
    port map (i_sys_clk_fast => i_sys_clk_fast, -- : in std_logic;
              i_sys_rst      => i_sys_rst, -- : in std_logic;
              -----------------------------------------------------------------------
              i_all_data_ready  => i_all_data_ready,
              i_all_nodes_empty => i_all_nodes_empty,
              i_all_bconv_ready => all_bconv_ready,
              i_all_bconv_done  => all_bconv_done,
              o_bconv_done      => bconv_done_array(0),
              o_bconv_ready     => bconv_ready_array(0),
              -- earlier on the path -------------------------------------------------
              i_src_payload     => i_hdr_payload, -- : in  std_logic_vector;
              i_src_wr_strb     => i_hdr_wr_strb, -- : in std_logic;
              i_src_nempty      => i_hdr_nempty,
              i_src_data_ready  => i_hdr_data_ready,
              o_src_full        => o_hdr_full,
              -- later on the path --------------------------------------------------
              i_dst_ce          =>      i_pbldr_ce_array(0),
              i_dst_pfull       =>   i_pbldr_pfull_array(0), -- : out std_logic;
              o_dst_wr_strb     => o_pbldr_wr_strb_array(0), -- : in  std_logic);
              o_dst_payload     => o_pbldr_payload_array(0), -- : out std_logic_vector;
              o_dst_nempty      =>  o_pbldr_nempty_array(0));


  GEN_BCONV: for jj in 1 to STREAMS generate
    signal stream_hdr_payload : std_logic_vector(high(NBITS_STREAM_HDR_ARRAY, jj-1) downto low(NBITS_STREAM_HDR_ARRAY, jj-1));
    signal stream_body_payload : std_logic_vector(high(g_STREAM_WIDTH_ARRAY, jj-1) downto low(g_STREAM_WIDTH_ARRAY, jj-1));
  begin

    G0 : if i_stream_hdr_payload_emurray'ascending = true generate
     stream_hdr_payload <=  i_stream_hdr_payload_emurray(low(NBITS_STREAM_HDR_ARRAY, jj-1) to high(NBITS_STREAM_HDR_ARRAY, jj-1));
    else generate
     stream_hdr_payload <=  i_stream_hdr_payload_emurray(high(NBITS_STREAM_HDR_ARRAY, jj-1) downto low(NBITS_STREAM_HDR_ARRAY, jj-1));
    end generate G0;
     
    G1 : if i_stream_body_payload_emurray'ascending = true generate
      stream_body_payload <= i_stream_body_payload_emurray(low(  g_STREAM_WIDTH_ARRAY, jj-1) to high(  g_STREAM_WIDTH_ARRAY, jj-1));
    else generate
      stream_body_payload <= i_stream_body_payload_emurray(high(  g_STREAM_WIDTH_ARRAY, jj-1) downto low(  g_STREAM_WIDTH_ARRAY, jj-1));
    end generate G1;

    --* bus converter between the header of data nodes (stream headers) and the packet
    --* builders
    u_bconv_dnode_hdr : entity daq_core.daq_bus_conv
      generic map (g_FIFO_DEPTH => 16, g_FIFO_TYPE => "auto", g_GHDL => g_GHDL)
      port map (i_sys_clk_fast => i_sys_clk_fast,
                i_sys_rst      => i_sys_rst,
                -----------------------------------------------------------------------
                i_all_data_ready  => i_all_data_ready,
                i_all_nodes_empty => i_all_nodes_empty,
                i_all_bconv_ready => all_bconv_ready,
                i_all_bconv_done  => all_bconv_done,
                o_bconv_done      => bconv_done_array(2*jj-1),
                o_bconv_ready     => bconv_ready_array(2*jj-1),
                -- earlier on the path -------------------------------------------
                i_src_payload     => stream_hdr_payload, 
                i_src_wr_strb     => i_stream_hdr_wr_strb_array(jj-1),
                i_src_nempty      => i_stream_hdr_nempty_array(jj-1),
                i_src_data_ready  => i_stream_hdr_data_ready_array(jj-1),
                o_src_full        =>   o_stream_hdr_full_array(jj-1),
                -- later on the path -------------------------------------------
                i_dst_ce          =>      i_pbldr_ce_array(2*jj-1),
                i_dst_pfull       =>   i_pbldr_pfull_array(2*jj-1), 
                o_dst_wr_strb     => o_pbldr_wr_strb_array(2*jj-1),
                o_dst_payload     => o_pbldr_payload_array(2*jj-1),
                o_dst_nempty      =>  o_pbldr_nempty_array(2*jj-1));
  
  
    --* bus converter between the data nodes (streams body) and the packet
    --* builders
    u_bconv_dnode_body : entity daq_core.daq_bus_conv
      generic map (g_FIFO_DEPTH => g_FIFO_DEPTH_array(jj-1), g_FIFO_TYPE => g_FIFO_TYPE, g_GHDL => g_GHDL)
      port map (i_sys_clk_fast => i_sys_clk_fast, 
                i_sys_rst      => i_sys_rst,
                -----------------------------------------------------------------------
                i_all_data_ready  => i_all_data_ready,
                i_all_nodes_empty => i_all_nodes_empty,
                i_all_bconv_ready => all_bconv_ready,
                i_all_bconv_done  => all_bconv_done,
                o_bconv_done      => bconv_done_array(2*jj),
                o_bconv_ready     => bconv_ready_array(2*jj),
                -- early on the path -------------------------------------------
                i_src_payload     => stream_body_payload, 
                i_src_wr_strb     => i_stream_body_wr_strb_array(jj-1), 
                i_src_nempty      => i_stream_body_nempty_array(jj-1),
                i_src_data_ready  => i_stream_body_data_ready_array(jj-1),
                o_src_full        => o_stream_body_full_array(jj-1),
                -- later on the path -------------------------------------------
                i_dst_ce          =>      i_pbldr_ce_array(2*jj),
                i_dst_pfull       =>   i_pbldr_pfull_array(2*jj), 
                o_dst_wr_strb     => o_pbldr_wr_strb_array(2*jj),
                o_dst_payload     => o_pbldr_payload_array(2*jj),
                o_dst_nempty      => o_pbldr_nempty_array(2*jj));
  
  end generate GEN_BCONV;


end architecture V2;


