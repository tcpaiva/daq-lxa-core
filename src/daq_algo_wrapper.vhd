-- file created for resource assessment only

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library daq_core;
use daq_core.daq_v3.all;
use daq_core.daq_defs.all;
use daq_core.yml2hdl.all;

entity daq_algo_wrapper is
  generic (
    g_PIPELINES                         : integer        := 1;
    g_STREAM_WIDTH_ARRAY                : integer_vector := (0 => 64);
    g_FIFO_DEPTH_ARRAY                  : integer_vector := (0 => 65536);
    g_FIFO_MEMORY_TYPE_ARRAY            : string_vector  := (0 => "auto");
    g_NBITS_STREAM_HDR_FIFO_DEPTH_ARRAY : integer_vector := (0 => 16);
    g_NBITS_STREAM_HDR_WIDTH_ARRAY      : integer_vector := (0 => 16);
    g_NBITS_STREAM_HDR_COUNTER_ARRAY    : integer_vector := (0 => 16);
    g_NBITS_STREAM_HDR_USER_ARRAY       : integer_vector := (0 => 16);
    g_NBITS_SNAPSHOT                    : integer        := 64;
    g_OUTPUT_LINKS                      : integer        := 1;
    g_PACKET_BUILDER_FIFO_DEPTH         : integer        := 16;
    g_PACKET_BUILDER_FIFO_TYPE          : string         := "auto";
    g_FORMAT_VERSION                    : natural        := 1;
    g_BUS_CONVERTER_FIFO_TYPE           : string         := "auto";
    g_GHDL                              : boolean        := false
  );
  port (
    i_sys_clk_fast                 : in std_logic;
    i_sys_rst                      : in std_logic;
    i_sys_bx                       : in std_logic;
    i_sys_hdr                      : in std_logic_vector(31 downto 0);
    o_wme_available                : out std_logic;
    i_req_strb                     : in std_logic;
    i_win_opening_cnt              : in unsigned(11 downto 0);
    i_win_request_cnt              : in unsigned(11 downto 0);
    i_win_closing_cnt              : in unsigned(11 downto 0);
    i_win_opening_offset           : in unsigned(11 downto 0);
    i_win_request_offset           : in unsigned(11 downto 0);
    i_win_closing_offset           : in unsigned(11 downto 0);
    i_win_window_timeout           : in unsigned(11 downto 0);
    i_win_busy_threshold           : in unsigned(07 downto 0);
    o_busy                         : out std_logic;
    i_bcid_cnt                     : in unsigned(11 downto 0);
    i_event_id                     : in unsigned(31 downto 0);
    i_curr_bcid                    : in unsigned(11 downto 0);
    i_usr_hdr_payload              : in std_logic_vector(15 downto 0);
    i_stream_user_header_emurray   : in std_logic_vector(15 downto 0);
    i_stream_ctrl_bcid_array       : bcid_array_t(0 to g_STREAM_WIDTH_ARRAY'length-1);
    i_stream_data_bcid_array       : bcid_array_t(0 to g_STREAM_WIDTH_ARRAY'length-1);
    i_stream_valid_array           : std_logic_vector(0 to g_STREAM_WIDTH_ARRAY'length-1);
    i_stream_enable_array          : std_logic_vector(0 to g_STREAM_WIDTH_ARRAY'length-1);
    i_stream_payload_emurray       : std_logic_vector(sum(g_STREAM_WIDTH_ARRAY)-1 downto 0);
    i_f2e_pfull_array              : in  std_logic_vector(0 to g_OUTPUT_LINKS-1);
    o_f2e_ctrl_array               : out std_logic_vector_array(0 to g_OUTPUT_LINKS-1)(1 downto 0);
    o_f2e_wr_strb_array            : out std_logic_vector(0 to g_OUTPUT_LINKS-1);
    o_f2e_payload_array            : out std_logic_vector_array(0 to g_OUTPUT_LINKS-1)(7 downto 0)
  );
end entity daq_algo_wrapper;

architecture V2 of daq_algo_wrapper is
begin
  u_daq_algo : entity daq_core.daq_algo
  generic map (
    g_PIPELINES                         => g_PIPELINES                        ,
    g_STREAM_WIDTH_ARRAY                => g_STREAM_WIDTH_ARRAY               ,
    g_FIFO_DEPTH_ARRAY                  => g_FIFO_DEPTH_ARRAY                 ,
    g_FIFO_MEMORY_TYPE_ARRAY            => g_FIFO_MEMORY_TYPE_ARRAY           ,
    g_NBITS_STREAM_HDR_FIFO_DEPTH_ARRAY => g_NBITS_STREAM_HDR_FIFO_DEPTH_ARRAY,
    g_NBITS_STREAM_HDR_WIDTH_ARRAY      => g_NBITS_STREAM_HDR_WIDTH_ARRAY     ,
    g_NBITS_STREAM_HDR_COUNTER_ARRAY    => g_NBITS_STREAM_HDR_COUNTER_ARRAY   ,
    g_NBITS_STREAM_HDR_USER_ARRAY       => g_NBITS_STREAM_HDR_USER_ARRAY      ,
    g_NBITS_SNAPSHOT                    => g_NBITS_SNAPSHOT                   ,
    g_PACKET_BUILDER_FIFO_DEPTH         => g_PACKET_BUILDER_FIFO_DEPTH        ,
    g_PACKET_BUILDER_FIFO_TYPE          => g_PACKET_BUILDER_FIFO_TYPE         ,
    g_FORMAT_VERSION                    => g_FORMAT_VERSION                   ,
    g_BUS_CONVERTER_FIFO_TYPE           => g_BUS_CONVERTER_FIFO_TYPE          ,
    g_GHDL                              => g_GHDL         
  )
  port map (
    i_sys_clk_fast               => i_sys_clk_fast              ,   
    i_sys_rst                    => i_sys_rst                   ,  
    i_sys_bx                     => i_sys_bx                    ,  
    i_sys_hdr                    => i_sys_hdr                   ,  
    o_wme_available              => o_wme_available             ,  
    i_req_strb                   => i_req_strb                  ,  
    i_win_opening_cnt            => i_win_opening_cnt           ,  
    i_win_request_cnt            => i_win_request_cnt           ,  
    i_win_closing_cnt            => i_win_closing_cnt           ,  
    i_win_opening_offset         => i_win_opening_offset        ,  
    i_win_request_offset         => i_win_request_offset        ,  
    i_win_closing_offset         => i_win_closing_offset        ,  
    i_win_window_timeout         => i_win_window_timeout        ,  
    i_win_busy_threshold         => i_win_busy_threshold        ,  
    o_busy                       => o_busy                      ,  
    i_bcid_cnt                   => i_bcid_cnt                  ,  
    i_event_id                   => i_event_id                  ,  
    i_curr_bcid                  => i_curr_bcid                 ,  
    i_usr_hdr_payload            => i_usr_hdr_payload           ,  
    i_stream_user_header_emurray => i_stream_user_header_emurray,  
    i_stream_ctrl_bcid_array     => i_stream_ctrl_bcid_array    ,  
    i_stream_data_bcid_array     => i_stream_data_bcid_array    ,  
    i_stream_valid_array         => i_stream_valid_array        ,  
    i_stream_enable_array        => i_stream_enable_array       ,  
    i_stream_payload_emurray     => i_stream_payload_emurray    ,  
    i_f2e_pfull_array            => i_f2e_pfull_array           ,  
    o_f2e_ctrl_array             => o_f2e_ctrl_array            ,  
    o_f2e_wr_strb_array          => o_f2e_wr_strb_array         ,  
    o_f2e_payload_array          => o_f2e_payload_array           
  );

end architecture V2;
