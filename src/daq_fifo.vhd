library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

--* @author  Thiago Costa de Paiva
--* @id      $git$
--* @version $git$
--*
--* @brief Wrapper to facilitate and standardize the use of the XPM Synchronous
--* First-word-falls-through FIFO from Xilinx. XPM FIFO is used instead of the
--* related IP core. FIFO "full", "almost full", and "half full" flags are
--* exposed.

entity daq_fifo is
--------------------------------------------------------------------------------
  generic (
    --+ FIFO description
    g_FIFO_DEPTH       : integer;
    g_FIFO_MEMORY_TYPE :  string;
    g_GHDL             : boolean
  );
  ------------------------------------------------------------------------------
  port (
    --* FIFO operating clock. It says "fast" to reference the single clock
    --* domain used in the design.
    i_sys_clk_fast        : in         std_logic;
    --* system reset
    i_sys_rst             : in         std_logic;
    ------------------------------------------------------------------------
    --* FIFO almost full flag. It is raised when all but one slots of the FIFO
    --* are used.
    o_src_afull           : out        std_logic;
    --* FIFO half full flag. It is raised when 68.75% of g_FIFO_DEPTH is reached.
    o_src_pfull           : out        std_logic;
    --* FIFO full flag. It is raised when the FIFO is fully occupied.
    o_src_full            : out        std_logic;
    --* Content to be stored in FIFO. Width of payload is defined by the signal
    --* connected to it using the VHDL2008 style.
    i_src_payload         : in  std_logic_vector;
    --* FIFO write strobe
    i_src_wr_strb         : in  std_logic       ;
    ------------------------------------------------------------------------
    --* FIFO read strobe. This signal should be acted after the output payload
    --* is used.
    i_dst_rd_strb         : in         std_logic;
    --* FIFO nempty flag. It is raised when there is data available in the FIFO.
    o_dst_nempty          : out        std_logic;
    --* written words counter. reset to zero when empty is reached.
    o_dst_wr_cnt          : out        unsigned;
    --* Content read from FIFO. Width of payload is defined by the signal
    --* connected to it using the VHDL2008 style.
    o_dst_payload         : out std_logic_vector
  );
--------------------------------------------------------------------------------
end entity daq_fifo;

architecture V1 of daq_fifo is

  --* Number of bits for the internal FIFO write counter. The write counter
  --* itself is not used in this design since it is fully disabled by the
  --* advanced features configuration of the XPM FIFO.
  constant WRITE_DATA_COUNT_WIDTH : integer := integer(log2(real(g_FIFO_DEPTH)));

  component fwft is
    generic (
      g_DEPTH : natural;
      g_PFULL_FACTOR : real := 0.7
      );
    port (
      i_clk     : in  std_logic;
      i_rst     : in  std_logic;
      ---------------------------------
      i_payload : in  std_logic_vector;
      i_wr_strb : in  std_logic;
      o_pfull   : out std_logic;
      o_afull   : out std_logic;
      o_full    : out std_logic;
      o_cnt     : out unsigned;
      --------------------------------
      i_rd_strb : in  std_logic;
      o_payload : out std_logic_vector;
      o_empty   : out std_logic
      );
  end component fwft;

  component daq_xpm_fifo is
    generic (g_FIFO_DEPTH       : integer;
             g_FIFO_MEMORY_TYPE :  string);
    port (i_sys_clk_fast : in         std_logic;
          i_sys_rst      : in         std_logic;
          o_src_afull    : out        std_logic;
          o_src_pfull    : out        std_logic;
          o_src_full     : out        std_logic;
          i_src_payload  : in  std_logic_vector;
          i_src_wr_strb  : in         std_logic;
          i_dst_rd_strb  : in         std_logic;
          o_dst_nempty   : out        std_logic;
          o_dst_payload  : out std_logic_vector;
          o_dst_wr_cnt   : out         unsigned);
  end component daq_xpm_fifo;

begin

  GEN_FIFO : if g_GHDL = false generate
  begin

    u_fifo : component daq_xpm_fifo
      generic map (g_FIFO_DEPTH       => g_FIFO_DEPTH,
                   g_FIFO_MEMORY_TYPE => g_FIFO_MEMORY_TYPE)
      port map (i_sys_clk_fast => i_sys_clk_fast,
                i_sys_rst      => i_sys_rst,
                o_src_afull    => o_src_afull,
                o_src_pfull    => o_src_pfull,
                o_src_full     => o_src_full,
                i_src_payload  => i_src_payload,
                i_src_wr_strb  => i_src_wr_strb,
                i_dst_rd_strb  => i_dst_rd_strb,
                o_dst_payload  => o_dst_payload,
                o_dst_wr_cnt   => o_dst_wr_cnt,
                o_dst_nempty   => o_dst_nempty);


    else generate
      signal empty : std_logic;
    begin

      u_fwft : component fwft
        generic map (g_DEPTH => g_FIFO_DEPTH)
        port map (i_clk     => i_sys_clk_fast,
                  i_rst     =>      i_sys_rst,
                  i_payload =>  i_src_payload,
                  i_wr_strb =>  i_src_wr_strb,
                  o_pfull   =>    o_src_pfull,
                  o_afull   =>    o_src_afull,
                  o_full    =>     o_src_full,
                  o_cnt     =>   o_dst_wr_cnt,
                  i_rd_strb =>  i_dst_rd_strb,
                  o_payload =>  o_dst_payload,
                  o_empty   =>          empty);
      o_dst_nempty <= '1' when empty = '0' else '0';

    end generate GEN_FIFO;

end architecture V1;
