import sys

filename = sys.argv[1]
start = int(sys.argv[2])
step = int(sys.argv[3])

cnt = start
with open(filename) as file:
    for line in file:
        evid = int(f"0x{line[9:17]}", 16)
        if cnt != evid: 
            print(f">>> {cnt:x}: {evid:x}")
        cnt += step
