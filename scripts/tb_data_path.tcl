quit -sim

file delete output0.txt
file delete output1.txt

set dir [file dirname [file normalize [info script]]]

vlib $dir/../proj/tb_data_path/work
vmap work $dir/../proj/tb_data_path/work

vlib $dir/../proj/tb_data_path/daq_core
vmap daq_core $dir/../proj/tb_data_path/daq_core

vcom -2008 -work daq_core $dir/../autogen/yml2hdl.vhd
vcom -2008 -work daq_core $dir/../autogen/daq_v3.vhd
vcom -2008 -work daq_core $dir/../pkg/daq_defs.vhd
vcom -2008 -work daq_core $dir/../pkg/daq_version.vhd


vlib msim
vlib msim/xpm
vmap xpm msim/xpm

vcom -work xpm /opt/Xilinx/Vivado/2022.2/data/ip/xpm/xpm_VCOMP.vhd
vlog -work xpm /opt/Xilinx/Vivado/2022.2/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv
vlog -work xpm /opt/Xilinx/Vivado/2022.2/data/ip/xpm/xpm_fifo/hdl/xpm_fifo.sv

vcom -2008 -work daq_core $dir/../src/daq_xpm_fifo.vhd
vcom -2008 -work daq_core $dir/../src/daq_fifo.vhd
vcom -2008 -work daq_core $dir/../src/daq_head_node.vhd
vcom -2008 -work daq_core $dir/../src/daq_data_node.vhd
vcom -2008 -work daq_core $dir/../src/daq_bus_conv.vhd
vcom -2008 -work daq_core $dir/../src/daq_wme_mux.vhd
vcom -2008 -work daq_core $dir/../src/daq_win_matching.vhd
vcom -2008 -work daq_core $dir/../src/daq_ptr_handler.vhd
vcom -2008 -work daq_core $dir/../src/daq_fifo.vhd
vcom -2008 -work daq_core $dir/../src/daq_packet_builder.vhd
vcom -2008 -work daq_core $dir/../src/daq_algo.vhd
vcom -2008 -work daq_core $dir/../src/daq_req.vhd
vcom -2008 $dir/../tb/tb_data_path.vhd

vsim -t ns -voptargs=+acc tb
# vsim -t ns tb


add wave -group top         *

add wave -group algo                       sim:/tb/u_output_data/*

add wave -group wm0         u_output_data.GEN_WM_ENGINES(0).u_wm.*
add wave -group wm0.hnode   u_output_data.GEN_WM_ENGINES(0).u_wm.u_head_node.*
add wave -group wm0.dnode0  u_output_data.GEN_WM_ENGINES(0).u_wm.GEN_DATA_NODES(0).u_data_node.*

add wave -group wm1         u_output_data.GEN_WM_ENGINES(1).u_wm.*
add wave -group wm1.hnode   u_output_data.GEN_WM_ENGINES(1).u_wm.u_head_node.*
add wave -group wm1.dnode0  u_output_data.GEN_WM_ENGINES(1).u_wm.GEN_DATA_NODES(0).u_data_node.*

add wave -group wm2         u_output_data.GEN_WM_ENGINES(2).u_wm.*
add wave -group wm2.hnode   u_output_data.GEN_WM_ENGINES(2).u_wm.u_head_node.*
add wave -group wm2.dnode0  u_output_data.GEN_WM_ENGINES(2).u_wm.GEN_DATA_NODES(0).u_data_node.*

# add wave -group wm3         u_output_data.GEN_WM_ENGINES(3).u_wm.*
# add wave -group wm3.hnode   u_output_data.GEN_WM_ENGINES(3).u_wm.u_head_node.*
# add wave -group wm3.dnode0  u_output_data.GEN_WM_ENGINES(3).u_wm.GEN_DATA_NODES(0).u_data_node.*

## add wave -group wm4         u_output_data.GEN_WM_ENGINES(4).u_wm.*
## add wave -group wm4.hnode   u_output_data.GEN_WM_ENGINES(4).u_wm.u_head_node.*
## add wave -group wm4.dnode0  u_output_data.GEN_WM_ENGINES(4).u_wm.GEN_DATA_NODES(0).u_data_node.*

# # add wave -group wm5         u_output_data.GEN_WM_ENGINES(5).u_wm.*
# # add wave -group wm5.hnode   u_output_data.GEN_WM_ENGINES(5).u_wm.u_head_node.*
# # add wave -group wm5.dnode0  u_output_data.GEN_WM_ENGINES(5).u_wm.GEN_DATA_NODES(0).u_data_node.*

# # add wave -group wm6         u_output_data.GEN_WM_ENGINES(6).u_wm.*
# # add wave -group wm6.hnode   u_output_data.GEN_WM_ENGINES(6).u_wm.u_head_node.*
# # add wave -group wm6.dnode0  u_output_data.GEN_WM_ENGINES(6).u_wm.GEN_DATA_NODES(0).u_data_node.*

add wave -group node_mux0   u_output_data.G0(0).u_node_mux.*
add wave -group node_mux0.bconv_hnode      u_output_data.G0(0).u_node_mux.u_bconv_hnode_fifo.*
add wave -group node_mux0.bconv_dnode_hdr  u_output_data.G0(0).u_node_mux.GEN_BCONV(1).u_bconv_dnode_hdr.*
add wave -group node_mux0.bconv_dnode_body u_output_data.G0(0).u_node_mux.GEN_BCONV(1).u_bconv_dnode_body.*

add wave -group pbldr0      u_output_data.G0(0).u_pbldr.*
add wave -group pbldr0.fifo u_output_data.G0(0).u_pbldr.u_fifo.GEN_FIFO.u_fifo.*

# add wave -group node_mux1   u_output_data.G0(1).u_node_mux.*
# add wave -group pbldr1      u_output_data.G0(1).u_pbldr.*

add wave -group req u_daq_req.*

add wave -group ptr_hdlr    u_output_data.u_ptr_handler.*

# add wave -group bconv_hnode u_output_data.G0(0).u_node_mux.u_bconv_hnode_fifo.*

# run 10us


