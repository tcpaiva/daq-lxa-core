SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

rm -f $SCRIPT_DIR/output*.txt

ghdl --clean

ghdl -a --work=daq_core --std=08  -frelaxed $SCRIPT_DIR/../autogen/yml2hdl.vhd
ghdl -a --work=daq_core --std=08  -frelaxed $SCRIPT_DIR/../autogen/daq_v3.vhd

ghdl -a --work=daq_core --std=08  -frelaxed $SCRIPT_DIR/../pkg/daq_defs.vhd

ghdl -a --work=daq_core --std=08  -frelaxed $SCRIPT_DIR/../tb/fwft.vhd

ghdl -a --work=daq_core --std=08  -frelaxed $SCRIPT_DIR/../src/daq_fifo.vhd
ghdl -a --work=daq_core --std=08  -frelaxed $SCRIPT_DIR/../src/daq_data_node.vhd
ghdl -a --work=daq_core --std=08  -frelaxed $SCRIPT_DIR/../src/daq_head_node.vhd
ghdl -a --work=daq_core --std=08  -frelaxed $SCRIPT_DIR/../src/daq_bus_conv.vhd
ghdl -a --work=daq_core --std=08  -frelaxed $SCRIPT_DIR/../src/daq_win_matching.vhd
ghdl -a --work=daq_core --std=08  -frelaxed $SCRIPT_DIR/../src/daq_ptr_handler.vhd
ghdl -a --work=daq_core --std=08  -frelaxed $SCRIPT_DIR/../src/daq_wme_mux.vhd
ghdl -a --work=daq_core --std=08  -frelaxed $SCRIPT_DIR/../src/daq_packet_builder.vhd
ghdl -a --work=daq_core --std=08  -frelaxed $SCRIPT_DIR/../src/daq_algo.vhd
ghdl -a --work=daq_core --std=08  -frelaxed $SCRIPT_DIR/../src/daq_req.vhd

ghdl -a --std=08  $SCRIPT_DIR/../tb/tb_data_path.vhd

ghdl -e --std=08 tb

ghdl -r --std=08 tb --wave=wave.ghw
