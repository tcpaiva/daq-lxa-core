quit -sim

set dir [file dirname [file normalize [info script]]]

vlib $dir/../proj/tb_pbldr/work
vmap work $dir/../proj/tb_pbldr/work

vlib $dir/../proj/tb_pbldr/daq_core
vmap daq_core $dir/../proj/tb_pbldr/daq_core

vcom -2008 -work daq_core $dir/../autogen/yml2hdl.vhd
vcom -2008 -work daq_core $dir/../autogen/daq_v3.vhd
vcom -2008 -work daq_core $dir/../pkg/daq_defs.vhd
vcom -2008 -work daq_core $dir/../src/daq_packet_builder.vhd
vcom -2008 $dir/../tb/tb_pkt_bldr.vhd

vsim -t ns -voptargs=+acc tb_pkt_bldr

add wave sys_clk_fast
add wave sys_rst
add wave enable
add wave nempty_array
add wave wr_strb_array
add wave u_pkt_bldr/wr_strb_array_d1
add wave payload_array
add wave busy_array

add wave f2e_wr_strb
add wave f2e_payload
add wave f2e_ctrl

run 2us


