set SCRIPT_PATH [file dirname [file normalize [info script ]]]

read_vhdl -vhdl2008 -library daq_core $SCRIPT_PATH/../autogen/yml2hdl.vhd
read_vhdl -vhdl2008 -library daq_core $SCRIPT_PATH/../autogen/daq_v3.vhd

read_vhdl -vhdl2008 -library daq_core $SCRIPT_PATH/../pkg/daq_defs.vhd
read_vhdl -vhdl2008 -library daq_core $SCRIPT_PATH/../pkg/daq_version.vhd

read_vhdl -vhdl2008 -library daq_core $SCRIPT_PATH/../src/daq_xpm_fifo.vhd
read_vhdl -vhdl2008 -library daq_core $SCRIPT_PATH/../src/daq_fifo.vhd
read_vhdl -vhdl2008 -library daq_core $SCRIPT_PATH/../src/daq_head_node.vhd
read_vhdl -vhdl2008 -library daq_core $SCRIPT_PATH/../src/daq_data_node.vhd
read_vhdl -vhdl2008 -library daq_core $SCRIPT_PATH/../src/daq_ptr_handler.vhd
read_vhdl -vhdl2008 -library daq_core $SCRIPT_PATH/../src/daq_win_matching.vhd
read_vhdl -vhdl2008 -library daq_core $SCRIPT_PATH/../src/daq_bus_conv.vhd
read_vhdl -vhdl2008 -library daq_core $SCRIPT_PATH/../src/daq_wme_mux.vhd
read_vhdl -vhdl2008 -library daq_core $SCRIPT_PATH/../src/daq_packet_builder.vhd
read_vhdl -vhdl2008 -library daq_core $SCRIPT_PATH/../src/daq_algo.vhd
read_vhdl -vhdl2008 -library daq_core $SCRIPT_PATH/../src/daq_req.vhd
