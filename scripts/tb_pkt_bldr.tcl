quit -sim

set dir [file dirname [file normalize [info script]]]

vlib $dir/../proj/tb_pbldr/work
vmap work $dir/../proj/tb_pbldr/work

vlib $dir/../proj/tb_pbldr/daq_core
vmap daq_core $dir/../proj/tb_pbldr/daq_core

vcom -2008 -work daq_core $dir/../autogen/yml2hdl.vhd
vcom -2008 -work daq_core $dir/../autogen/daq_v3.vhd
vcom -2008 -work daq_core $dir/../pkg/daq_defs.vhd
vcom -2008 -work daq_core $dir/../src/daq_fifo.vhd
vcom -2008 -work daq_core $dir/../src/daq_packet_builder.vhd
vcom -2008 $dir/../tb/tb_pkt_bldr.vhd

vsim -t ns -voptargs=+acc tb_pkt_bldr

add wave -group tb *
add wave -group dut u_pkt_bldr.*

run 2us


