import sys

filename = sys.argv[1]

with open(filename, 'r') as f:
    for ii, line in enumerate(f):
        line = line.strip()
        line = [line[i:i+4] for i in range(0, len(line), 4)]
        crc = 0
        for ll in line[0:-1]:
            crc = crc ^ int(ll,16)
        if f"{crc:04x}".lower() != line[-1].lower():
            print(f"line {ii+1}: {line[-1]} {crc:04x} ")
        #if ii == 6:
        #    break
