SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

ghdl --clean

ghdl -a --work=daq_core --std=08 $SCRIPT_DIR/../autogen/yml2hdl.vhd
ghdl -a --work=daq_core --std=08 $SCRIPT_DIR/../autogen/daq_v3.vhd

ghdl -a --work=daq_core --std=08 $SCRIPT_DIR/../pkg/daq_defs.vhd

ghdl -a --work=daq_core --std=08 $SCRIPT_DIR/../tb/fwft.vhd
ghdl -a --work=daq_core --std=08 $SCRIPT_DIR/../src/daq_fifo.vhd
ghdl -a --work=daq_core --std=08 $SCRIPT_DIR/../src/daq_req.vhd
ghdl -a --std=08 $SCRIPT_DIR/../tb/tb_req.vhd

ghdl -e --std=08 tb_req

ghdl -r --std=08 tb_req --wave=wave.ghw
