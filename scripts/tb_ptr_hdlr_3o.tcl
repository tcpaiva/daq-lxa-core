quit -sim

set dir [file dirname [file normalize [info script]]]

vlib $dir/../proj/tb_ptr_hdlr/work
vmap work $dir/../proj/tb_ptr_hdlr/work

vlib $dir/../proj/tb_ptr_hdlr/daq_core
vmap daq_core $dir/../proj/tb_ptr_hdlr/daq_core

vcom -2008 -work daq_core $dir/../autogen/yml2hdl.vhd
vcom -2008 -work daq_core $dir/../autogen/daq_v3.vhd
vcom -2008 -work daq_core $dir/../pkg/daq_defs.vhd
vcom -2008 -work daq_core $dir/../src/daq_ptr_handler.vhd
vcom -2008 $dir/../tb/tb_ptr_hdlr.vhd

vsim -t ns -voptargs=+acc tb_ptr_hdlr

add wave sys_clk_fast
add wave sys_rst

add wave sys_clk_slow  
add wave sys_clk_slow_d
add wave sys_bx 

add wave sys_rst

add wave req_strb              
add wave req_stable            
add wave wm_active_array       
add wave wm_rd_wme_ptr_vector  
add wave wm_rd_pbldr_ptr_vector
add wave wm_req_strb_array     

add wave overflow_cnt
add wave use_snapshot

run 5us


