library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library daq_core;
use daq_core.all;
use daq_core.yml2hdl.all;

package daq_defs is

  function get_max(x: integer_vector) return integer;
  function sum(x: integer_vector) return integer;

  function high(SIZES: integer_vector; idx : integer) return integer;
  function low(SIZES: integer_vector; idx: integer) return integer ;

  function get_stream_payload (payload_emu_array: std_logic_vector;
                       SIZES: integer_vector;
                       idx: integer) return std_logic_vector;

  type string_vector is array(natural range <>) of string;
  type unsigned_vector is array(integer range <>) of unsigned;

  type felix_data_rt is record
    hfull : std_logic;
    wr_strb : std_logic;
    ctrl    : std_logic_vector(1 downto 0);
    payload : std_logic_vector(31 downto 0);
  end record felix_data_rt;

  type felix_data_art is array(integer range <>) of felix_data_rt;
  
end package daq_defs; 

package body daq_defs is

  function get_max(x: integer_vector) return integer is
    variable max : integer := x(x'low);
  begin
    for j in x'range loop
      if max < x(j) then
        max := x(j);
      end if;
    end loop;
    return max;
  end function get_max;

  function sum(x: integer_vector) return integer is
    variable s : integer := 0; 
  begin
    for j in x'range loop
      s := s + x(j);
    end loop;
    return s;
  end function;

  function high(SIZES: integer_vector; idx : integer) return integer is
    variable pos : integer := 0;
  begin
    for j in 0 to idx loop
      pos := pos + SIZES(j);
    end loop;
    return pos - 1;
  end function;
  
  function low(SIZES: integer_vector; idx: integer) return integer  is
    variable pos : integer := 0;
  begin
    for j in 0 to idx - 1 loop
      pos := pos + SIZES(j);
    end loop;
    return pos;
  end function;

  function get_stream_payload (payload_emu_array: std_logic_vector;
                               SIZES: integer_vector;
                               idx: integer) return std_logic_vector is
    variable msb : natural := high(SIZES, idx);
    variable lsb : natural := low(SIZES, idx);
  begin
    if payload_emu_array'ascending then      
      return payload_emu_array(lsb to msb); 
    else
      return payload_emu_array(msb downto lsb);
    end if;
  end function get_stream_payload;
    
end package body daq_defs;
