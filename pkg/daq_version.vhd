library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;

package daq_version is
  constant c_DAQ_LXA_CORE_MAJOR  : unsigned(7 downto 0) := to_unsigned(1, 8);
  constant c_DAQ_LXA_CORE_MINOR  : unsigned(7 downto 0) := to_unsigned(5, 8);
  constant c_DAQ_LXA_CORE_PATCH  : unsigned(7 downto 0) := to_unsigned(5, 8);
  constant c_DAQ_LXA_CORE_PARENT : std_logic_vector(27 downto 0) := x"4b00aa1";
end package daq_version;
