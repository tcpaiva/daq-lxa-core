\newcommand{\lxastrb}{\se{lxa_strb}\xspace}
\newcommand{\request}{\se{request}\xspace}
\newcommand{\opening}{\se{opening}\xspace}
\newcommand{\closing}{\se{closing}\xspace}

\chapter{Introduction}

\section{Algorithm description}

The \daq \lxa is a generic and flexible concurrent timestamp-based data selection and packet formatting implementation for \fpga devices, that can be adapted for different contexts given some basic precepts. Many parameters can be adjusted and will be described in detail later, but number and width of the input data streams, timestamp search range, timestamp offsets, number of simultaneous searches could be mentioned as examples.

An input data word, correlated to a timestamp and a valid bit, is only accepted when two simultaneous conditions are met: the timestamp has to be part of a range of accepted values while the acquisition period is active. Both, the timestamp range and the acquisition period, are prepared according parameters that can be configurable at runtime. It is worth emphasizing that, as the data timestamp is provided in a different channel from the data word itself, the \daq \lxa implementation do not need to know the structure of the word, just its width. More details about this solution and how to use it will be provided below.

The time diagram in Figure \ref{fig: overview time diagram} illustrates how data selection happens. When a data request is received (request strobe), all search parameters get defined: the first (opening counter) and last (closing counter) timestamps of the range, as well as its reference (request counter) timestamp. At same time (green region), the control timestamp input is observed in order to detect the start value of the range indicating the opening of the search window. As one could expect, the search window remains open until the last value of the range is not presented by the control timestamp. While the search window is active (blue region), the data timestamp is checked for values in the previously defined range (yellow regions) if the data is valid (red region). Therefore, when there is an overlap of the green, blue, red and yellow regions, the related data word is stored, which is represented by a black dot.

\begin{figure}[!hbtp]
  \centering
  \includegraphics[width=\linewidth]{img/window-params}
  \caption{Time diagram example with DAQ LXA operation overview}
  \label{fig: overview time diagram}
\end{figure}

The opening, closing, and request counter values are calculated according to configurable offsets relative to the system timestamp, that is, how many time units each one of them is behind of the system timestamp. In the case shown in Figure \ref{fig: overview time diagram}, the opening offset is 50, the request offset is 48, and the closing offset is 44. These offsets are needed considering that the data acquisition request takes a given number of slow clock cycles to be processed and emitted.

Other than that, the only signal missing explanation in Figure \ref{fig: overview time diagram} is the timed out flag. It is there to ensure that the system will not malfunction if for some reason the opening or closing values are not presented on the control timestamp input, interrupting the search window after a defined number of slow clock cycles. In the example, timeout is 10. It is worth pointing out that the timeout value should be large enough to accommodate the initial waiting time plus the full length of the search window.

It is easy to conclude that the data timestamp can be also used as the control timestamp if (a) the data timestamp is sequential with no gaps (difference of more than one unit), and (b) every timestamp is active for at least one fast clock cycle (valid flag do not need to be active).

\section{Basic assumptions}

\begin{itemize}
\item One and only fast clock domain used (tested with \SI{320}{\mega\hertz}).
\item Signals related to the slow clock domain should be provided as a strobe in the fast clock domain.
\item A high-level reset signal required during initialization for one or more clock cycles.
\item Data buffering to wait for the acquisition request should be prepared by user (not included in this implementation), since different projects have different requirements.
\end{itemize}

\section{Search window}


When a acquisition request is received, a search window is calculated so that each word timestamp can be compared to. It is important to consider that it is not convenient to design the size of the input data buffers to be exactly the size needed to synchronize data with acquisition request, because changing the buffer size would require the design to be re-implemented, which could take several hours. A different approach is to design the buffering stage with some slack and provide the means to slide the search window to the point where the data can be found. The way this implementation solves the synchronization between buffered data and the acquisition request is using configurable offsets relative to the system timestamp when calculating opening, request, and closing counters.

The calculation of the request-related timestamps is intuitive. The current timestamp is converted to a timestamp in the past based on the configured offset. The greater the offset, the more in the past the corresponding timestamp will be. This is also true for the opening and closing offsets, meaning that opening offsets have to be greater than the request offset, which on its turn is greater than the closing offset. Using the example illustrated by Figure \ref{fig: overview time diagram},  it is easy to understand that opening counter is 351, request counter is 253, closing timestamp is 357, since the system timestamp is 401 and offsets are, respectively, 50, 48, and 44.


\section{Files}

The files below are described in a top-down perspective because it is easier to visualize the implementation. This means that even if a block was not mentioned before, the explanation about it should be just after that point. All packages should be included as part of the \se{daq-core} library. All files are written in VHDL-2008.

\subsection{Packages}
\begin{description}
\item [\se{daq_defs}:] package with some support functions.
\item [\se{daq_v3}:] package with constants, data structure, and convert functions. This file is generated using the \se{yml2hdl} (\url{https://gitlab.com/tcpaiva/yml2hdl}) tool with the \se{daq_v3.yml} as source.
\end{description}

\subsection{Source}

\begin{description}
\item [\se{daq_req.vhd}:] entity to calculate search windows ranges and acquisition timestamps. It is a required block common to all the \daq \lxa instances.
\item [\se{daq_algo.vhd}:] data interface to the other parts of the design, it instantiates the window matching engines and connect them to the packet builder. Read and write index of the window matching engines are managed here.
\item [\se{daq_win_matching.vhd}:] is where header and data nodes are instantiated to store information to be sent out.
\item [\se{daq_head_node.vhd}:] store common information of the packet header.
\item [\se{daq_data_node.vhd}:] implement the logic to select data and hold a FIFO instance to store input words of interest.
\item [\se{daq_node_fifo.vhd}:] ``first-word-falls-through'' symmetrical FIFOs are used to store the selected data. There is a counter for the number of words written, which is also part of the packet.
\item [\se{daq_packet_builder.vhd}:] waits for all data ready signals (one for each data stream and header nodes) are active. Retrieve information starting from header node. Calculates \crc for the packet, which is appended as the last word of the packet after all nodes are empty.
\item [\se{daq_bus_conv.vhd}:] bus width conversion is required since each data stream can have different number of bits. It can be configured as wide-to-narrow, narrow-to-wide, or no conversion.
\end{description}


\section{High-level process overview}

There is a high-level step-by-step description of the search process below:

\begin{enumerate}
\item Acquisition request is received
\item Search window parameters are calculated.
\item A window matching engine is allocated.
\item Common information of the package is stored in the head node.
\item Each data stream is exposed to a data nodes that will watch for interesting data and store them.
\item When the the last timestamp is found on the control counter, the search is over and data ready is signalized. Each stream will emit this signal independently.
\item Once all nodes signalize that information is ready, the packet builder is activated.
\item The packet builder sweep all nodes, one at a time starting from the head, and retrieve data available until nodes are empty. For each data stream, data words are sliced and serialized to the output.
\item \crc is appended to the packet
\item Packet builder moves to the next window matching and waits to be activated again.
\end{enumerate}


\chapter{Interface description}


\section{Naming conventions}
\begin{itemize}
\item Generic parameter names start with \se{g_}.
\item Input signal names start with \se{i_}.
\item Output signal names start with \se{o_}.
\item Strobe signals are suffixed with \se{_strb}.
\item Counter signals are suffixed with \se{_cnt}.
\item Array signals are suffixed with \se{_array}.
\item Bit vectors that emulate array of bit vectors with different width members are suffixed with \se{_emu_array}.
\end{itemize}


\section{User-required Entities}

\subsection{Basic system signals}

Common signals to the following entities, request processing and \daq \lxa algorithm.

\begin{description}
\item [\se{i_sys_clk}:] fast clock signal.
\item [\se{i_sys_rst}:] reset clock signal
\item [\se{i_sys_bx}:] strobe signal indicating the pace of the slow clock domain.
\end{description}

\subsection{Request processing block}

\subsubsection{Request processing entity declaration}

\lstinputlisting{lst/req_interface.txt}

\subsubsection{Generics}

\begin{description}
\item [\se{g_OPENING_OFFSET}:] integer offset for calculation of the opening counter used as the lower limit of the search range. Defaults to \se{DAQ_DEFAULT_OPENING_OFFSET}.
\item [\se{g_REQUEST_OFFSET}:] integer offset for calculation of the request counter used as the request timestamp. Defaults to \se{DAQ_DEFAULT_REQUEST_OFFSET}.
\item [\se{g_CLOSING_OFFSET}:] integer offset for calculation of the closing counter used as the upper limit of the search range. Defaults to \se{DAQ_DEFAULT_CLOSING_OFFSET}.
\item [\se{g_TIMEOUT_WINDOW}:] integer value indicating how long, in total, the search window will be active at most.
\end{description}

By design

\[
  \se{g_OPENING_OFFSET} \ge \se{g_REQUEST_OFFSET} \ge \se{g_CLOSING_OFFSET}
\]

since the opening timestamp has to happen earlier than (or at same time of) the request timestamp, which in turns has to happen earlier than (or at same time of) the closing timestamp.

\subsubsection{Timestamp and event control signals}

\begin{description}
\item [\se{i_lxa_strb}:] input strobe signal representing the acquisition request.
\item [\se{i_bcr_strb}:] input strobe signal indicating when system timestamp should reset.
\item [\se{i_ecr_strb}:] input rstrobe signal indicating when event counter should reset.
\item [\se{o_event_id}:] output event counter used in packet header.
\end{description}

\subsubsection{Search window parameters}


\begin{description}
\item [\se{i_ctrl_opening_offset}:] integer offset for calculation of the opening counter used as the lower limit of the search range. Runtime configuration.
\item [\se{i_ctrl_request_offset}:] integer offset for calculation of the request counter used as the request timestamp. Runtime configuration.
\item [\se{i_ctrl_closing_offset}:] integer offset for calculation of the closing counter used as the upper limit of the search range. Runtime configuration.
\item [\se{i_ctrl_timeout_window}:] integer value indicating how long, in total, the search window will be active at most. Runtime configuration.
\item [\se{o_status_stable}:] when any of the search window parameters changes at runtime (previous items), the \daq \lxa implementation enters a temporary state where no acquisition request is accepted to recalculate the opening, request, and closing counters. This is indicated by this flag.
\item [\se{o_status_valid}:] flag to indicate if search window parameters used are valid, which means that the window limits can be set properly.
\end{description}

\subsubsection{Interface with window matching block}

Signals below are exposed to be connected to the \daq \lxa algorithm block, since just one request processing instance should be used.

\begin{description}
\item [\se{o_wm_start_strb}:] strobe signal indicating that an acquisition request was received in a valid context.
\item [\se{o_wm_opening_cnt}:] opening counter for the lower limit of the search window.
\item [\se{o_wm_request_cnt}:] request counter used as the acquisition request timestamp.
\item [\se{o_wm_closing_cnt}:] closing counter for the upper limit of the search window.
\item [\se{o_wm_timeout}:] timeout value to interrupt the search window.
\item [\se{o_wm_opening_offset}:] opening offset value used in the packet header (verification purposes).
\item [\se{o_wm_request_offset}:] request offset value used in the packet header (verification purposes).
\item [\se{o_wm_closing_offset}:] closing offset value used in the packet header (verification purposes).
\item [\se{o_wm_timeout_window}:] timeout value used in the packet header (verification purposes).
\end{description}

\subsection{DAQ LXA algorithm block}

\subsubsection{DAQ LXA algorithm entity declaration}

\lstinputlisting{lst/algo_interface.txt}

\subsubsection{Generic parameters}

\begin{description}
\item [\se{g_PIPELINES}:] number of acquisition requests that can be processed simultaneously.
\item [\se{g_STREAM_WIDTH_ARRAY}:] array of integers describing the width of the data streams.
\item [\se{g_FIFO_DEPTH_ARRAY}:] array of integers describing the depth of the data FIFO to be used.
\item [\se{g_FIFO_MEMORY_TYPE_ARRAY}:] type of memory to be used to implement the FIFO (0: auto, 1: BRAM, 2: distributed RAM, 3: URAM).
\item [\se{g_NBITS_STREAM_HDR_FIFO_DEPTH_ARRAY}:] array of integers describing the number of bits for each FIFO depth in packet header.
\item [\se{g_NBITS_STREAM_HDR_WIDTH_ARRAY}:]  array of integers describing the number of bits for each data stream width in packet header.
\item [\se{g_NBITS_STREAM_HDR_COUNTER_ARRAY}:]  array of integers describing the number of bits for each counter width in packet header.
\item [\se{g_NBITS_STREAM_HDR_USER_ARRAY}:]  array of integers describing the number of bits for the user field in packet header.
\end{description}

\subsubsection{Packet header signals}

\begin{description}
\item [\se{i_sys_hdr}:] Unconstrained array used for the first part of the header, usually enforced by external systems. 
\item [\se{i_usr_hdr_payload}:] Unconstrained array for any user general information to be inserted in the packet. It should be empty (\se{i_usr_hdr_payload => ""}) if not used.
\end{description}

\subsubsection{Interface with request processing block}

\begin{description}
\item [\se{i_req_start_strb}:] it should be connected to \se{o_wm_start_strb} from request processing block (see above).
\item [\se{i_req_stable}:] it should be connected to \se{o_status_stable} from request processing block (see above).
\item [\se{i_win_opening_cnt}:] it should be connected to \se{o_wm_opening_cnt} from request processing block (see above).
\item [\se{i_win_request_cnt}:] it should be connected to \se{o_wm_request_cnt} from request processing block (see above).
\item [\se{i_win_closing_cnt}:] it should be connected to \se{o_wm_closing_cnt} from request processing block (see above).
\item [\se{i_win_timeout}:] it should be connected to \se{o_wm_timeout} from request processing block (see above).
\item [\se{i_win_opening_offset}:] it should be connected to \se{o_wm_opening_offset} from request processing block (see above).
\item [\se{i_win_request_offset}:] it should be connected to \se{o_wm_request_offset} from request processing block (see above).
\item [\se{i_win_closing_offset}:] it should be connected to \se{o_wm_closing_offset} from request processing block (see above).
\item [\se{i_win_timeout}:] it should be connected to \se{o_wm_timeout} from request processing block (see above).
\end{description}

\subsubsection{Input data stream signals}

\begin{description}
\item [\se{i_stream_user_header_emu_array}:] concatenated user header bit vectors, one for each defined data stream.
\item [\se{i_stream_ctrl_timestamp_array}:] array of control timestamp unsigned vectors, one for each defined data stream.
\item [\se{i_stream_data_timestamp_array}:] array of data timestamp unsigned vectors, one for each defined data stream.
\item [\se{i_stream_valid_array}:] bit vector with all valid signals, one for each defined data stream. This signal should be active when data available on the input payload bus should be processed.
\item [\se{i_stream_enable_array}:] bit vector with all enable signals, one for each defined data stream. This allow data streams to be enabled or disabled at runtime.
\item [\se{i_stream_payload_emu_array}:] concatenated data stream payload bit vectors, one for each defined data stream.
\end{description}

\subsubsection{Output interface}

The output uses a FIFO-like interface. 

\begin{description}
\item [\se{i_f2e_hfull}:] signal to prevent data to be written to the next stage.
\item [\se{o_f2e_ctrl}:]  2-bit control signal to indicate status of communication (00: idle, 10: start of packet, 11: sending, 01: end of packet).  
\item [\se{o_f2e_wr_strb}:] strobe to write data to the next stage.
\item [\se{o_f2e_payload}:] data to be written to the next stage. It is defined as a unconstrained array, therefore width will be given by signal connected to it.
\end{description}

\subsubsection{CRC field}

After all headers and data are inserted in the packet, a last word is appended with a very simple \crc information. This is intended only to give the user some confidence over the data transmission. For instance, if the \crc field is wrong, something happened after the packet left the \daq \lxa implementation. Otherwise, please open an issue.

To avoid long logic chains -- which would make the place and route process difficult --, the \crc is calculated between output words of the packet. For example, if the output payload bus is 16 bits wide, for every 16-bit word that is written to the next stage, each bit is XORed with the corresponding index bit from the previous word.

\chapter{Summary of user tasks}

The recommended steps for the use this implementation are:

\begin{enumerate}
\item Decide about how many output interfaces are required. They act independently and will define the number of algorithm instances needed.
\item Decide on how many data streams will be provided by each algorithm instance. This has to take into consideration the output bandwidth.
\item Prepare buffering stage for the data that will be provided as data streams. It should be long enough to allow the acquisition request signal to be delivered plus the length of the search window defined.
\item Convert all signals to the fast clock domain.
\item (optional) Write a data acquisition entity to interface all the entities required by the \daq \lxa implementation. This will help user code to be more organized.
\item Instantiate a request processing block (\se{daq_req}). 
\item Create one instance of the acquisition algorithm block (\se{daq_algo}) for each output interface required.
\item Interconnect the request processing unit to all the acquisition algorithm available.
\end{enumerate}

\chapter{Packet formatting}


\begin{description}
\item [System required header:] user defined, unconstrained
\item [L1A common info:] 128 bits, see below
  \begin{description}
  \item [window opening timestamp:] 12 bits
  \item [l1a request timestamp:] 12 bits
  \item [window closing timestamp:] 12 bits
  \item [overflow counter:] 12 bits
  \item [window matching engines usage:] 32 bits
  \item [window opening offset configured:] 12 bits
  \item [l1a request offset configured:] 12 bits
  \item [window closing offset configured:] 12 bits
  \item [timeout configured:] 12 bits
  \end{description}
\item [Global user header:] user defined, unconstrained
\item [Packet payload:] variable, depends on data collected, see below
\begin{description}
  \item [stream 0:] variable, see below
    \begin{description}
    \item [stream 0 header:] user defined, see below
      \begin{description}
      \item [number of bits of stream payload:] user defined by generic parameter
      \item [number of words collected:] user defined by generic parameter
      \item [FIFO depth:] user defined by generic parameter
      \end{description}
    \item [stream 0 user header:] user defined, unconstrained
    \item [stream 0 data:] variable, given by the selected data. Number of words indicated in the stream header above.
    \end{description}
  \item [stream 1:] same as stream 0, it continues on until all data streams are included
  \item [<other streams>]: ...
  \end{description}
\item [Packet trailer:] \se{XORed} word with the same width of the output payload bus.
\end{description}



\chapter{Final remarks}

\begin{itemize}
\item The \daq \lxa implementation was designed to be used as a git submodule. It can be accessed by \url{https://gitlab.com/tcpaiva/daq-lxa-core}.
\item Data streams where no data was selected, will be represented by its header with zeroed number of words.
\item Width of unconstrained vectors are defined by the signals connected to them.
\item Reminder: All packages should be included as part of the \se{daq-core} library.
\item Reminder: All files require VHDL 2008 standard.
\end{itemize}