library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------------------------------------------------------------

entity fwft is
  generic (
    g_DEPTH : natural;
    g_PFULL_FACTOR : real := 0.7
  );
  port (
    i_clk     : in  std_logic;
    i_rst     : in  std_logic;
    ---------------------------------
    i_payload : in  std_logic_vector;
    i_wr_strb : in  std_logic;
    o_pfull   : out std_logic;
    o_afull   : out std_logic;
    o_full    : out std_logic;
    o_cnt  : out unsigned;
    --------------------------------
    i_rd_strb : in  std_logic;
    o_payload : out std_logic_vector;
    o_empty   : out std_logic
  );
end entity fwft;

-------------------------------------------------------------------------------

architecture str of fwft is

  type mem_t is array (0 to g_DEPTH-1) of std_logic_vector(i_payload'range);
  signal mem : mem_t := (others => (others => '1'));

  signal wr_ptr, rd_ptr : natural := 0;
  signal wr_trn, rd_trn : boolean := false;

  signal cnt : unsigned(o_cnt'range) := (others => '0');

  signal full, empty, empty_d : std_logic;

  signal a, b, c, d : std_logic_vector(0 to 1);
  
begin

  a(0) <= '1' when wr_ptr > rd_ptr else '0';
  a(1) <= '1' when wr_ptr - rd_ptr >= natural(g_PFULL_FACTOR * real(g_DEPTH)) else '0';
  b(0) <= '1' when wr_ptr < rd_ptr else '0';
  b(1) <= '1' when wr_ptr + g_DEPTH - rd_ptr >= natural(g_PFULL_FACTOR * real(g_DEPTH)) else '0';
  o_pfull <= '1' when  a = "11" or b = "11" else '0';
  
  
  c(0) <= '1' when wr_ptr > rd_ptr else '0';
  c(1) <= '1' when wr_ptr - rd_ptr >= g_DEPTH-1 else '0';
  d(0) <= '1' when wr_ptr < rd_ptr else '0';
  d(1) <= '1' when wr_ptr + g_DEPTH - rd_ptr >= g_DEPTH-1 else '0';
  o_afull <= '1' when c = "11" or d = "11" else '0';
  
  mem(wr_ptr) <= i_payload when rising_edge(i_clk) and i_wr_strb = '1' and full = '0';
  o_payload <= mem(rd_ptr);
  -- o_payload <= mem(rd_ptr) when rising_edge(i_clk) and i_rd_strb = '1';

  o_full <= full;
  full <= '1' when rd_ptr = wr_ptr and rd_trn /= wr_trn else '0';

  o_empty <= empty;
  empty <= '1' when rd_ptr = wr_ptr and rd_trn = wr_trn else '0';

  o_cnt <= cnt;
  process (i_clk)
    variable cnt_aux : unsigned(o_cnt'range);
  begin
    if rising_edge(i_clk) then
      empty_d <= empty;
      cnt_aux := cnt;
      if (empty = '1' and empty_d = '0') or i_rst = '1' then
        cnt_aux := (others => '0');
      end if;
      if i_wr_strb = '1' and i_rst = '0' then
        cnt_aux := cnt_aux + 1;
      end if;
      cnt <= cnt_aux;
    end if;
  end process;
  
  process (i_clk)
  begin
    if rising_edge(i_clk) then
      if i_wr_strb = '1' and full = '0' then
        if wr_ptr < g_DEPTH-1 then
          wr_ptr <= wr_ptr + 1;
        else
          wr_ptr <= 0;
          wr_trn <= not wr_trn;
        end if;
      end if;

      if i_rd_strb = '1' and empty = '0' then
        if rd_ptr < g_DEPTH-1 then
          rd_ptr <= rd_ptr + 1;
        else
          rd_ptr <= 0;
          rd_trn <= not rd_trn;
        end if;
      end if;
    end if; -- clk

  end process;
  
end architecture str;

