library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library daq_core;
use daq_core.yml2hdl.all;
use daq_core.daq_v3.all;
use daq_core.daq_defs.all;

entity tb_pkt_bldr is
end entity tb_pkt_bldr;

architecture RTL of tb_pkt_bldr is

  signal sys_clk_fast : std_logic := '1';
  signal sys_clk_fast_d : std_logic := '1';
  
  signal sys_rst : std_logic := '1';

  signal enable :   std_logic := '0';

  signal nempty_array     : std_logic_vector(0 to 8) := (others => '0');
  signal wr_strb_array    : std_logic_vector(0 to 8) := (others => '0');
  signal payload_array    : std_logic_vector_array(0 to 8)(31 downto 0);
  signal hfull_array      : std_logic_vector(0 to 8);
  signal ce_array         : std_logic_vector(0 to 8);
  signal data_ready_array : std_logic_vector(0 to 8) := (others => '1');

  signal f2e_hfull   : std_logic := '0';
  signal f2e_wr_strb : std_logic := '0';
  signal f2e_payload : std_logic_vector(31 downto 0);
  signal f2e_ctrl    : std_logic_vector(1 downto 0);
  
  procedure wait_n_clock_cycles(constant x   : natural;
                                signal   clk : std_logic) is
  begin
    for i in 1 to x loop
      wait until rising_edge(clk);
    end loop;
  end procedure wait_n_clock_cycles;

begin

  u_pkt_bldr : entity daq_core.daq_packet_builder
    generic map (g_STREAM_HDR_WIDTH_ARRAY => (16, 16, 16, 16), -- : integer_vector,
                 g_STREAM_WIDTH_ARRAY     => (16, 16, 16, 16),
                 g_FIFO_DEPTH             => 16) -- : integer_vector)
    port map (i_sys_clk_fast => sys_clk_fast, -- : in std_logic;
              i_sys_clk_slow => '0', -- : in std_logic;
              i_sys_rst      => sys_rst     , -- : in std_logic;
              -- control and monitoring ========================================
              i_enable => enable, -- : in  std_logic;
              -- nodes =========================================================
              o_ce_array      => ce_array,
              o_hfull_array    => hfull_array   , -- : out std_logic_vector;
              i_wr_strb_array => wr_strb_array, -- :  in std_logic_vector;
              i_payload_array => payload_array, -- :  in std_logic_vector_array;
              i_data_ready_array => data_ready_array,
              i_nempty_array => nempty_array,
              -- f2e ===========================================================
              i_f2e_hfull   => f2e_hfull  , -- : in  std_logic;
              o_f2e_wr_strb => f2e_wr_strb, -- : out std_logic;
              o_f2e_payload => f2e_payload, -- : out std_logic_vector;
              o_f2e_ctrl    => f2e_ctrl   ); -- : out std_logic_vector(1 downto 0));

  sys_clk_fast <= not sys_clk_fast after 5 ns;

  process
  begin
    wait_n_clock_cycles(3, sys_clk_fast);
    sys_rst <= '0';
    wait_n_clock_cycles(10, sys_clk_fast);
    nempty_array  <= (others => '1');
    wait_n_clock_cycles(5, sys_clk_fast);
    enable <= '1';
    wait_n_clock_cycles(10, sys_clk_fast);
    nempty_array  <= (0 to 0 => '0', others => '1');
    wait_n_clock_cycles(10, sys_clk_fast);
    nempty_array  <= (0 to 1 => '0', others => '1');
    wait_n_clock_cycles(10, sys_clk_fast);
    nempty_array  <= (0 to 2 => '0', others => '1');
    wait_n_clock_cycles(10, sys_clk_fast);
    nempty_array  <= (0 to 3 => '0', others => '1');
    wait_n_clock_cycles(10, sys_clk_fast);
    nempty_array  <= (0 to 4 => '0', others => '1');
    wait_n_clock_cycles(10, sys_clk_fast);
    nempty_array  <= (0 to 5 => '0', others => '1');
    wait_n_clock_cycles(10, sys_clk_fast);
    nempty_array  <= (0 to 6 => '0', others => '1');
    wait_n_clock_cycles(10, sys_clk_fast);
    nempty_array  <= (0 to 7 => '0', others => '1');
    wait_n_clock_cycles(10, sys_clk_fast);
    nempty_array  <= (0 to 8 => '0');
    wait on f2e_ctrl until f2e_ctrl = "01";
    enable <= '0';
    wait;
  end process;

  process(sys_clk_fast)
    variable seed1, seed2 :integer := 999;
    variable r1, r2 : unsigned(31 downto 0);
    variable r : unsigned(31 downto 0);

    impure function rand_int(min_val, max_val : natural) return natural is
      variable r : real;
    begin
      uniform(seed1, seed2, r);
      return integer(round(r * real(max_val - min_val + 1) + real(min_val) - 0.5));
    end function;
    
    impure function rand_slv(len : integer) return std_logic_vector is
      variable r : real;
      variable slv : std_logic_vector(len - 1 downto 0);
    begin
      for i in slv'range loop
        uniform(seed1, seed2, r);
        slv(i) := '1' when r > 0.5 else '0';
      end loop;
      return slv;
    end function;

  begin
    if rising_edge(sys_clk_fast) then
      for ii in 0 to 8 loop
        r1 := to_unsigned(rand_int(0, 2**16-1), 32);
        r2 := to_unsigned(rand_int(0, 2**16-1), 32);
        r  := r1 + r2;
        payload_array(ii) <= std_logic_vector(r);
      end loop;
      wr_strb_array <= rand_slv(9);
    end if;
  end process;

  
end architecture RTL;
