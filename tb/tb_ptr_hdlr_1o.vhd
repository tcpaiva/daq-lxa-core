library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library daq_core;
use daq_core.daq_v3.all;
use daq_core.daq_defs.all;

entity tb_ptr_hdlr is
end entity tb_ptr_hdlr;

architecture RTL of tb_ptr_hdlr is

  procedure wait_n_clock_cycles(constant x   : natural;
                                signal   clk : std_logic) is
  begin
    for i in 1 to x loop
      wait until rising_edge(clk);
    end loop;
  end procedure wait_n_clock_cycles;

  signal sys_clk_fast : std_logic := '1';

  signal sys_clk_slow   : std_logic := '1';
  signal sys_clk_slow_d : std_logic := '0';

  signal sys_rst      : std_logic := '1';
  signal sys_bx       : std_logic := '0';

  signal req_strb               : std_logic := '0';
  signal req_stable             : std_logic := '1';
  signal wm_active_array        : std_logic_vector(0 to 6) := (others => '0');
  signal wm_rd_wme_ptr_vector   : integer_vector(0 to 0);
  signal wm_rd_pbldr_ptr_vector : integer_vector(0 to 6);
  signal wm_req_strb_array      : std_logic_vector(0 to 6);
  
  signal overflow_cnt : unsigned(7 downto 0);
  signal use_snapshot : std_logic_vector(0 to 6);

begin

  u_daq_ptr_hdlr : entity daq_core.daq_ptr_handler
    generic map (g_PIPELINES    => 7, -- : natural;
                 g_OUTPUT_LINKS => 1) -- : natural);
    port map (i_sys_clk_fast => sys_clk_fast, -- : in std_logic;
              i_sys_clk_slow => '0'         , -- : in std_logic;
              i_sys_rst      => sys_rst     , -- : in std_logic;
              i_sys_bx       => sys_bx      , -- : in std_logic;
              -- Request strobe, read and write pointers =============================
              i_req_strb               => req_strb              , -- : in  std_logic;
              i_req_stable             => req_stable            , -- : in  std_logic;
              i_wm_active_array        => wm_active_array       , -- : in  std_logic_vector;
              o_wm_rd_wme_ptr_vector   => wm_rd_wme_ptr_vector  , -- : out integer_vector;
              o_wm_rd_pbldr_ptr_vector => wm_rd_pbldr_ptr_vector, -- : out integer_vector;
              o_wm_req_strb_array      => wm_req_strb_array     , -- : out std_logic_vector;
              -- Monitoring ==========================================================
              o_overflow_cnt => overflow_cnt, -- : out unsigned;
              o_use_snapshot => use_snapshot); -- : out std_logic_vector);

  
  sys_clk_fast <= not sys_clk_fast after 5 ns;

  process (sys_clk_fast)
    variable cnt : natural := 0;
  begin
    if cnt = 7 then
      sys_clk_slow <= not sys_clk_slow;
      cnt := 0;
    else
      cnt := cnt + 1;
    end if;
    sys_clk_slow_d <= sys_clk_slow;
  end process;

  sys_bx <= sys_clk_slow and not sys_clk_slow_d;
  
  
   -- process(sys_clk_fast)
   -- begin
   --  if rising_edge(sys_clk_fast) then
   --      if req_strb = '1' then
   --          wm_active_array <= std_logic_vector(unsigned(wm_active_array) + 1);
   --      end if;
   --  end if;
   -- end process;
  
  process
  begin
    req_strb <= '0';
    wait_n_clock_cycles(1, sys_clk_fast);
    req_strb <= '1';
    wait_n_clock_cycles(1, sys_clk_fast);
    req_strb <= '0';
    wait_n_clock_cycles(1, sys_clk_fast);
    sys_rst <= '0';
    wait_n_clock_cycles(10, sys_clk_fast);
    req_strb <= '1';
    wait_n_clock_cycles(1, sys_clk_fast);
    req_strb <= '0';
    wait_n_clock_cycles(10, sys_clk_fast);
    req_strb <= '1';
    wait_n_clock_cycles(1, sys_clk_fast);
    req_strb <= '0';
    wait_n_clock_cycles(10, sys_clk_fast);
    req_strb <= '1';
    wait_n_clock_cycles(1, sys_clk_fast);
    req_strb <= '0';
    wait_n_clock_cycles(10, sys_clk_fast);
    req_strb <= '1';
    wait_n_clock_cycles(1, sys_clk_fast);
    req_strb <= '0';
    wait_n_clock_cycles(10, sys_clk_fast);
    req_strb <= '1';
    wait_n_clock_cycles(1, sys_clk_fast);
    req_strb <= '0';
    wait_n_clock_cycles(10, sys_clk_fast);
    req_strb <= '1';
    wait_n_clock_cycles(1, sys_clk_fast);
    req_strb <= '0';
    wait_n_clock_cycles(10, sys_clk_fast);
    req_strb <= '1';
    wait_n_clock_cycles(1, sys_clk_fast);
    req_strb <= '0';
    wait_n_clock_cycles(10, sys_clk_fast);
    req_strb <= '1';
    wait_n_clock_cycles(1, sys_clk_fast);
    req_strb <= '0';
    wait_n_clock_cycles(10, sys_clk_fast);
    req_strb <= '1';
    wait_n_clock_cycles(1, sys_clk_fast);
    req_strb <= '0';
    wait_n_clock_cycles(10, sys_clk_fast);
    req_strb <= '1';
    wait_n_clock_cycles(1, sys_clk_fast);
    req_strb <= '0';
    wait_n_clock_cycles(10, sys_clk_fast);
    req_strb <= '1';
    wait_n_clock_cycles(1, sys_clk_fast);
    req_strb <= '0';
    wait_n_clock_cycles(10, sys_clk_fast);
    req_strb <= '1';
    wait_n_clock_cycles(1, sys_clk_fast);
    req_strb <= '0';
    wait_n_clock_cycles(10, sys_clk_fast);
    req_strb <= '1';
    wait_n_clock_cycles(1, sys_clk_fast);
    req_strb <= '0';
    wait_n_clock_cycles(10, sys_clk_fast);
    req_strb <= '1';
    wait_n_clock_cycles(1, sys_clk_fast);
    req_strb <= '0';
    wait_n_clock_cycles(10, sys_clk_fast);
    req_strb <= '1';
    wait_n_clock_cycles(1, sys_clk_fast);
    req_strb <= '0';
    wait_n_clock_cycles(10, sys_clk_fast);
    req_strb <= '1';
    wait_n_clock_cycles(1, sys_clk_fast);
    req_strb <= '0';
    wait_n_clock_cycles(10, sys_clk_fast);
    req_strb <= '1';
    wait_n_clock_cycles(1, sys_clk_fast);
    req_strb <= '0';
    wait;
  end process;

  G0: for ii in wm_active_array'range generate
    process (sys_clk_fast)
      variable cntdwn : integer;
    begin
      if rising_edge(sys_clk_fast) then
        if wm_req_strb_array(ii) = '1' then
          cntdwn := 90;
          wm_active_array(ii) <= '1';
        end if;
        if cntdwn > 0 then
          cntdwn := cntdwn - 1;
        end if;
        if cntdwn = 0 then
          wm_active_array(ii) <= '0';
        end if;
      end if;
    end process;
  end generate G0;
  
end architecture RTL;
