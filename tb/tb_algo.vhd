library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library daq_core;
use daq_core.daq_v3.all;
use daq_core.daq_defs.all;

entity tb_req is
end entity tb_req;

architecture RTL of tb_req is

  signal sys_clk_fast : std_logic := '1';
  signal sys_clk_fast_d : std_logic;
  
  signal sys_clk_slow : std_logic := '1';
  signal sys_clk_slow_d : std_logic := '1';
  
  signal sys_rst : std_logic := '1';
  signal sys_bx  : std_logic;

  signal lxa_strb : std_logic;
  signal bcr_strb : std_logic;
  signal ecr_strb : std_logic;
  signal bcid_cnt : unsigned(11 downto 0) := (others => '0');
  signal event_id : unsigned(31 downto 0);

  signal ctrl_wr_en          : std_logic;
  signal ctrl_opening_offset : unsigned(11 downto 0) := to_unsigned(30, 12);
  signal ctrl_request_offset : unsigned(11 downto 0) := to_unsigned(20, 12);
  signal ctrl_closing_offset : unsigned(11 downto 0) := to_unsigned(10, 12);
  signal ctrl_timeout_window : unsigned(11 downto 0) := to_unsigned(80, 12);

  signal status_stable : std_logic;
  signal status_valid  : std_logic;

  signal wm_wr_strb        : std_logic;
  signal wm_opening_cnt    : unsigned(11 downto 0);
  signal wm_request_cnt    : unsigned(11 downto 0);
  signal wm_closing_cnt    : unsigned(11 downto 0);
  signal wm_timeout_cnt    : unsigned(11 downto 0);
  signal wm_opening_offset : unsigned(11 downto 0);
  signal wm_request_offset : unsigned(11 downto 0);
  signal wm_closing_offset : unsigned(11 downto 0);
  signal wm_timeout_window : unsigned(11 downto 0);

  procedure wait_n_clock_cycles(constant x   : natural;
                                signal   clk : std_logic) is
  begin
    for i in 1 to x loop
      wait until rising_edge(clk);
    end loop;
  end procedure wait_n_clock_cycles;
  
begin

  u_daq_req : entity daq_core.daq_req
  port map (i_sys_clk_fast => sys_clk_fast, -- : in std_logic;
            i_sys_clk_slow => sys_clk_slow, -- : in std_logic;
            i_sys_rst      => sys_rst     , -- : in std_logic;
            i_sys_bx       => sys_bx      , -- : in std_logic;
            ------------------------------------------------------------------------
            i_lxa_strb => lxa_strb, -- : in  std_logic;
            i_bcr_strb => bcr_strb, -- : in  std_logic;
            i_ecr_strb => ecr_strb, -- : in  std_logic;
            i_bcid_cnt => bcid_cnt, -- : in  unsigned(11 downto 0);
            o_event_id => event_id, -- : out unsigned(31 downto 0);
            ------------------------------------------------------------------------
            i_ctrl_wr_en          => ctrl_wr_en         , -- : in std_logic;
            i_ctrl_opening_offset => ctrl_opening_offset, -- : in unsigned(11 downto 0);
            i_ctrl_request_offset => ctrl_request_offset, -- : in unsigned(11 downto 0);
            i_ctrl_closing_offset => ctrl_closing_offset, -- : in unsigned(11 downto 0);
            i_ctrl_timeout_window => ctrl_timeout_window, -- : in unsigned(11 downto 0);
            ------------------------------------------------------------------------
            o_status_stable => status_stable, -- : out std_logic;
            o_status_valid  => status_valid , -- : out std_logic;
            ------------------------------------------------------------------------
            o_wm_wr_strb        => wm_wr_strb       , -- : out std_logic;
            o_wm_opening_cnt    => wm_opening_cnt   , -- : out unsigned(11 downto 0);
            o_wm_request_cnt    => wm_request_cnt   , -- : out unsigned(11 downto 0);
            o_wm_closing_cnt    => wm_closing_cnt   , -- : out unsigned(11 downto 0);
            o_wm_timeout_cnt    => wm_timeout_cnt   , -- : out unsigned(11 downto 0);
            o_wm_opening_offset => wm_opening_offset, -- : out unsigned(11 downto 0);
            o_wm_request_offset => wm_request_offset, -- : out unsigned(11 downto 0);
            o_wm_closing_offset => wm_closing_offset, -- : out unsigned(11 downto 0);
            o_wm_timeout_window => wm_timeout_window); -- : out unsigned(11 downto 0));

  sys_clk_fast <= not sys_clk_fast after 5ns;

  process (sys_clk_fast)
    variable cnt : natural := 0;
  begin
    if cnt = 7 then
      sys_clk_slow <= not sys_clk_slow;
      cnt := 0;
    else
      cnt := cnt + 1;
    end if;
    sys_clk_slow_d <= sys_clk_slow;
  end process;

  sys_bx <= sys_clk_slow and not sys_clk_slow_d;
  
  process
  begin
    lxa_strb <= '0';
    wait_n_clock_cycles(1, sys_clk_fast);
    lxa_strb <= '1';
    wait_n_clock_cycles(1, sys_clk_fast);
    lxa_strb <= '0';
    wait_n_clock_cycles(1, sys_clk_fast);
    sys_rst <= '0';
    wait_n_clock_cycles(10, sys_clk_fast);
    lxa_strb <= '1';
    wait_n_clock_cycles(1, sys_clk_fast);
    lxa_strb <= '0';
    wait;
  end process;

  process(sys_clk_slow)
  begin
    if rising_edge(sys_clk_slow) then
      bcid_cnt <= bcid_cnt + 1;
    end if;
  end process;
  
  
end architecture RTL;
